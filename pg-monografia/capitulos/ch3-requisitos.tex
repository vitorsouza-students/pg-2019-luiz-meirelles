% ==============================================================================
% TCC - Luiz Meirelles
% Capítulo 3 - Especificação de Requisitos
% ==============================================================================

\chapter{Especificação de Requisitos}
\label{sec-requisitos}

A Engenharia de Requisitos tem com objetivo não somente levantar funcionalidades necessárias a aplicação, mas em entender o negócio, com objetivo final de entregar um produto que atenda às expectativas dos \textit{stakeholders}.

Neste capítulo é feita uma descrição de escopo do SCAP (Sistema de Controle de Afastamentos de Professores), depois tem-se os modelos de casos de uso que foram observados a partir dos requisitos previamente levantados por \citeonline{duarte-pg14} e \citeonline{prado-pg15}.

\section{Descrição do Escopo}
\label{sec-descricao-escopo}

No SCAP devem estar todas as solicitações de afastamentos dos professores do Departamento de Informática (DI). As solicitações podem ser para eventos no Brasil ou no exterior. Essa solicitações precisam ser avaliadas pelos professores do DI e, em alguns casos, pelo Conselho Departamental do Centro Tecnológico (CT) e pela Pró-reitoria de Pesquisa e Pós-Graduação (PRPPG) para que sejam aprovadas e o professor possa realizar a viagem.

Os pedidos para afastamento no Brasil, são aprovados pela Câmara Departamental (formada pelos funcionários do departamento e representantes discentes). O pedido é feito por meio da lista de e-mails dos funcionários do DI e endereçado ao chefe do departamento, cargo este que é exercido por algum professor do DI durante mandato temporário. Caso ninguém se manifeste contrário à solicitação, o pedido é aprovado. Destacando que para viagem no Brasil, o procedimento é inteiramente realizado dentro do DI.

No caso de viagem para o exterior é escolhido um professor (que não tenha parentesco com o solicitante) para ser o relator do pedido. Após o parecer do relator, o pedido passa pela aprovação do DI, igual no caso acima. No entanto, ainda é necessário que o CT e a PRPPG aprovem o pedido e que o afastamento seja publicado no Diário Oficial da União, conforme art. 95 da lei 8.112 (\url{http://www.planalto.gov.br/ccivil_03/leis/l8112cons.htm}).

O SCAP só trata das tramitações que são realizadas dentro do DI. Ele não tem nenhuma comunicação com os sistemas do CT e da PRPPG, a forma como eles tramitam os processos não faz parte do escopo do sistema. Solicitações para viagem no exterior fazem parte do escopo enquanto se encontram dentro do DI.

O SCAP foi criado para agilizar e facilitar a solicitação de afastamento para o professor. O sistema atinge o objetivo enviando e-mails automáticos aos envolvidos e do uso de formulários para ajudar na criação dos documentos necessários para se realizar uma solicitação. O sistema também permite que demais professores e secretários consultem dados sobre as solicitações.

\section{Modelo de Casos de Uso}
\label{sec-modelo-casos-uso}

O modelo de casos de uso visa capturar e descrever as funcionalidades que um sistema deve prover para os atores que interagem com o mesmo, isto é, os usuários do sistema~\cite{silva:2007}. Alguns casos de uso podem ser mais críticos que outros para a lógica de negócio do sistema. Após a definição do escopo, o processo de Engenharia de Requisitos seguiu com a modelagem de atores, demonstrado na Tabela~\ref{tbl-atores-scap}.

\begin{table}
\caption{Atores do SCAP.}
\label{tbl-atores-scap}
\centering
\begin{tabular}{|l|p{100mm}|}\hline
\textbf{Ator} & \textbf{Descrição}\\\hline
Professor & Professores Efetivos do DI/UFES.\\\hline 
Chefe do Departamento & Professores do DI/UFES que estão realizando a função administrativa de chefe e sub-chefe do departamento.\\\hline
Secretário & Secretário do DI/UFES.\\\hline 
\end{tabular}
\end{table}

O Secretário lida com a parte cadastral do sistema. Cadastro de professores e seus parentes. É de sua responsabilidade atualizar o Chefe do Departamento quando um novo é eleito. Casos de Uso como Registrar Parecer CT e Registrar Parecer PRPPG também são sua responsabilidade. Ele também é responsável por Arquivar uma Solicitação de Afastamento quando a mesma já foi regularizada.

O Professor cadastra sua Solicitação. Pode se manifestar contra a Solicitação de outro Professor e pode ser o Relator de uma Solicitação. O Chefe de Departamento pode executar todos Casos de Uso de um Professor e ainda encaminhar uma Solicitação ao seu Relator.

O SCAP foi dividido em 2 subsistemas: Núcleo e Secretaria. O primeiro contempla os casos de usos dos professores e do chefe de departamento, enquanto o segundo envolve os casos de uso dos secretários.

As figuras~\ref{fig-diagrama-casos-de-uso-do-nucleo} e~\ref{fig-diagrama-casos-de-uso-da-secretaria} mostram os diagramas de casos de uso destes subsistemas. Nos parágrafos que se seguem, apresentamos uma descrição sucinta dos casos de uso levantados para o SCAP. Uma versão mais detalhada dessa descrição pode ser vista em~\cite{duarte-pg14,prado-pg15}.

\begin{figure}
\centering 
\includegraphics[width=.7\textwidth]{../pg-monografia/figuras/diagrama-casos-de-uso-do-nucleo.png}
\caption{Diagrama de Casos de Uso do subsistema Núcleo~\cite{prado-pg15}.}
\label{fig-diagrama-casos-de-uso-do-nucleo}
\end{figure}

\begin{figure}
\centering 
\includegraphics[width=.7\textwidth]{../pg-monografia/figuras/diagrama-casos-de-uso-da-secretaria.png}
\caption{Diagrama de Casos de Uso do subsistema Secretaria~\cite{prado-pg15}.}
\label{fig-diagrama-casos-de-uso-da-secretaria}
\end{figure}

Em \textbf{Solicitar Afastamento}, um professor cadastra um pedido de afastamento no sistema, informando todos os dados necessários para a tramitação do mesmo. Em \textbf{Cancelar Afastamento} o professor cancela um pedido de afastamento e o seu status é alterado para cancelado, caso ele seja o solicitante.

O caso de uso \textbf{Encaminhar Afastamento} é realizado pelo Chefe do Departamento
quando o mesmo analisa um pedido de afastamento internacional recém cadastrado e aponta um professor como relator deste afastamento. Já \textbf{Deferir Parecer} é utilizado quando um professor, que foi cadastrado pelo Chefe do Departamento como relator de um afastamento internacional, cadastra seu parecer sobre o afastamento.

\textbf{Consultar Afastamento}, como o nome já diz, consiste em uma busca que um professor realiza para ver os dados de um pedido de afastamento. Em \textbf{Manifestar-se Contra Afastamento} um professor que é contrário a um afastamento cadastra o motivo de sua opinião contrária, assim uma reunião é marcada e os professores decidem se o afastamento é aprovado ou não.

Em \textbf{Cadastrar Usuário} um secretário cadastra um novo professor ou secretário no sistema, informando os dados pessoais necessários. \textbf{Em Cadastrar Chefe do Departamento} um secretário cadastra o mandato do Chefe de Departamento, informando as datas de início e fim do mesmo.

O casos de uso \textbf{Registrar Parecer CT} e \textbf{Registrar Parecer PRPPG} acontecem quando um secretário cadastra o parecer do Centro Tecnológico e da Pró-Reitoria de Pesquisa e Pós-Graduação, respectivamente, sobre um pedido de afastamento internacional. 

\textbf{Arquivar Afastamento} acontece no final da tramitação de um pedido de afastamento quando o secretário muda o status do mesmo para ``Arquivado''.

\section{Análise do SCAP}
\label{sec-analise-scap}

A atividade de Análise dentro do paradigma da Orientação a Objetos tem por objetivo identificar objetos do mundo real, características destes objetos e relacionamentos entre eles que são relevantes para o problema a ser resolvido, especificando e modelando o problema de forma que seja possível criar um projeto orientado a objetos efetivo~\cite{pressman:2005}. Durante a Análise do SCAP, os conceitos do domínio do problema foram descritos como classes, com seus respectivos atributos, interações e relações com outras classes.

A Modelagem de Classes segue o modelo levantado previamente por \citeonline{prado-pg15} ao fazer a modelagem para implementação do SCAP com o \textit{framework} VRaptor, revisando a modelagem feita anteriormente por \citeonline{duarte-pg14}. 

Apesar do SCAP ter sido dividido em dois subsistemas, as classes de domínio do problema, representadas no diagrama de classes da Figura~\ref{fig-diagrama-classes-scap}, todas pertencem ao subsistema Núcleo. Isso se dá porque a divisão em subsistemas ocorreu com intuito de facilitar a implementação, e não devido a natureza das classes criadas~\cite{prado-pg15}.

\begin{figure}[t]
\centering 
\includegraphics[width=.85\textwidth]{../pg-monografia/figuras/diagrama-classes-scap.png}
\caption{Diagrama de Classes do SCAP~\cite{prado-pg15}.}
\label{fig-diagrama-classes-scap}
\end{figure}

O Atributo ``situação da solicitação'' da classe Afastamento representa em qual estágio da tramitação um pedido de afastamento está, os status possíveis serão discutidos mais adiante. Já o atributo ``tipo do afastamento'' indica se o afastamento é necessário para atender a um evento localizado no Brasil ou no exterior.

As classes \textbf{Professor} e \textbf{Secretário} representam os professores de secretários do Departamento de Informática da UFES, respectivamente, ambas herdando os atributos da classe \textbf{Pessoa}. Professores podem possuir ou não uma relação de \textbf{Parentesco} com os demais professores. O chefe do departamento é representado por um \textbf{Professor} que possui um \textbf{Mandato} vigente.

Um \textbf{Afastamento} possui todas as informações necessárias para tramitação de um pedido de afastamento de um professor. A classe \textbf{Afastamento} pode ou não possuir mais de um \textbf{Documento} e requer um \textbf{Relator} somente quando o afastamento é devido a um evento internacional.

Um \textbf{Parecer} é emitido por um professor e é referente a um único afastamento, porém um professor pode criar vários pareceres para afastamentos diferentes, assim como um professor pode ser relator de vários afastamentos.

Restrições de Integridade complementam as informações de um modelo deste tipo e capturam restrições relativas a relacionamentos entre elementos de um modelo que normalmente não são passíveis de serem capturadas pelas notações gráficas utilizadas na elaboração de modelos conceituais estruturais. Tais regras devem ser documentadas junto ao modelo conceitual estrutural do sistema~\cite{falbo:2011}.

Listaremos aqui as restrições que foram observadas na primeira implementação do SCAP em~\cite{prado-pg15}, juntamente com novas restrições referentes a funcionalidades que não foram contempladas no primeira implementação do SCAP por \citeonline{duarte-pg14}:

\begin{itemize}
\item Um professor não pode ser solicitado para dar um parecer sobre sua própria solicitação de afastamento;
\item A data de início de um afastamento não pode ser posterior a data de fim do mesmo afastamento;
\item A data de início de um mandato de professor não pode ser posterior a data de fim do mesmo mandato;
\item Não pode haver mais de dois professores (chefe e subchefe de departamento) exercendo um mandato ao mesmo tempo;
\item O secretário do departamento não pode abrir uma solicitação de afastamento;
\item Um professor não pode ser relator de um afastamento solicitado por um parente.
\end{itemize}

