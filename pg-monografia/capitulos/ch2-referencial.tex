% ==============================================================================
% TCC - Luiz Meirelles
% Capítulo 2 - Engenharia Web e o Método FrameWeb
% ==============================================================================
\chapter{Engenharia Web e o Método FrameWeb}
\label{sec-webe-frameweb}
A Web modificou-se muito com os passar do tempo. Saiu de uma forma simples com páginas estáticas para páginas com comportamento dinâmico, oferecendo diversos serviços que estão presentes em nossa rotina. Agregou valor às nossas vidas, transformando a maneira de pensar, relacionar e viver. Nos últimos anos a Web tornou-se presente em muitas situações na vida de milhares de pessoas em todo mundo, ultrapassando muitos desenvolvimentos tecnológicos presentes na história~\cite{ginice-murugesan:2001}.

Presente em quase todo lugar, cada vez mais rápida e de fácil acesso, ela fornece serviços que se tornaram indispensáveis para nossa vida. E cada vez mais os sistemas antes desenvolvidos para um meio fechado e legados são migrados para a Web para se tornarem acessíveis. Diversos setores de negócios hoje em dia solicitam serviços que antes necessitavam de uma infraestrutura presente nas empresas, podendo hoje ser terceirizados, ex.: gerenciamento de e-mails, redes, máquinas virtuais, banco de dados e até serviço de desenvolvimento de aplicações Web (como \textit{chatbots}). Uma vasta abrangência de novas e complexas aplicações corporativas está emergindo na Web~\cite{li-et-all:2000}.

Tornou-se essencial a aplicação de conceitos existentes na Engenharia de Software,
adaptados para essa nova plataforma, no desenvolvimento das aplicações Web. Características do ambiente Web, como concorrência, carga imprevisível, disponibilidade, sensibilidade ao conteúdo, evolução dinâmica, imediatismo, segurança e estética~\cite{PRESSMAN:2011} deram origem a uma nova sub-área da Engenharia de Software, a Engenharia Web.

As aplicações estão se tornando cada vez mais complexas e mais presente nos dispositivos usuais, televisão, smartphones, relógios, carros, etc. Antigamente o usuário necessitava de ter a aplicação instalada localmente, hoje já é possível executá-la e ter o serviço necessário sem instalar nada, até mesmo sem ter um \textit{hardware} específico. Chamaremos aqui tais sistemas de WIS (\textit{Web-based Information Systems}) e ao conjunto amplo de todas as aplicações Web como WebApps (abreviação de \textit{Web Applications}). Tais aplicações executam em um servidor conectado à World Wide Web (WWW) e podem ser acessadas de qualquer computador ou dispositivo conectado à Internet.

Neste capítulo será abordada a adaptação da Engenharia de Software para satisfazer o desenvolvimento de WISs para, em seguida, demonstrar a aplicação do método FrameWeb para as WebApps e o benefício do uso de \textit{frameworks} para um rápido desenvolvimento.

\section{Engenharia Web}
\label{sec-webe-frameweb-webe}
A Engenharia Web (\textit{Web Engineering} -- WebE) é a Engenharia de Software voltada para o desenvolvimento de aplicações Web~\cite{pressman:2005}. A Engenharia Web trata de abordagens disciplinadas e sistemáticas para o desenvolvimento, implantação e manutenção de sistemas baseados na Web~\cite{desphande-et-al:2001}.

Como as aplicações se tornam cada vez mais complexas, necessitam de qualidade de desenvolvimento, por isso abordagens sistemáticas e disciplinadas são necessárias para manter um certo padrão de qualidade na construção da aplicação. Algumas aplicações são tão importantes para a organização que representam a fonte de renda da mesma, não podendo ficar indisponíveis como, por exemplo, lojas virtuais. Com toda essa complexidade, os problemas antes presentes em aplicações locais, também começam a aparecer nas aplicações para Web. Dificuldades como: inexperiência e formação inadequada dos desenvolvedores, falta (de uso) de métricas para estimativas, falta (de uso) de modelos de processo, métodos inadequados e obsoletos, planejamento incorreto, prazos e custos excedidos, falta de documentação, dificuldades de implementação e manutenção, WebApps mal definidas e projetadas, \textit{layout} inadequado (comunicação visual), mudança contínua dos requisitos, da tecnologia e da arquitetura, falta de rigor no processo de desenvolvimento, dentre outros~\cite{peruch-pg07}. Como podemos observar, a Engenharia Web está presente em todas as fases do desenvolvimento da aplicação.

Os mesmos conceitos ou atributos de qualidade~\cite{olsina-et-al:2001} empregados na Engenharia de Software também são necessários para o desenvolvimento da aplicação:
\begin{itemize}
    \item Usabilidade: o software deve ter uma característica inteligível, ou seja, fácil uso a todos os tipos de pessoas (inclusive deficientes), com pouco ou muito conhecimento técnico;
    \item Funcionalidade: o software deve se comportar bem atendendo todas as demandas para qual foi construído, buscando todas as informações corretamente e as operações funcionando;
    \item Eficiência: o tempo de resposta para determinada funcionalidade deve ser o mínimo possível. Atualmente também significa em como o sistema apresenta funcionalidades principais e enquanto busca as informações para os usuários. Por exemplo, ao invés de fazer o usuário esperar carregar o sistema inteiro, já carrega algumas funcionalidades mais utilizadas, permitindo-lhe ir direto ao ponto e apresentando o andamento do carregamento;
    \item Confiabilidade: o sistema deve se recuperar de erros, os links devem ser direcionados para os locais corretos, a validação das informações dos usuários devem ser checadas;
    \item Manutenibilidade: o sistema deve prover fácil manutenção para aprimorar funcionalidades e inserir novas ou retirar. Com novas atualizações, novas informações disponíveis no mercado, mudança nas regras de negócio e evolução das tecnologias, o sistema deve ser construído de forma fácil e intuitiva permitindo sua evolução ou adaptação.
\end{itemize}

Vários outros trabalhos foram e têm sido apresentados na literatura sobre como e o que deve ser avaliado em aplicações Web. Alguns desses trabalhos podem ser vistos em~\cite{bevan:1998,OlsinRossi1999pp,lowe:1999}.
Para se garantir esses atributos de qualidade um processo de desenvolvimento deve ser estabelecido e, conforme já mencionado anteriormente, a Engenharia Web já utiliza de métodos propostos pela Engenharia de Software~\cite{duarte-pg14}.

A Engenharia Web propõe uma abordagem incremental e iterativa, onde no final de cada iteração se realizam atividades de análise, projeto, implementação e testes numa parte do sistema, avaliando se o sistema corresponde às expectativas dos \textit{stakeholders}.
A primeira fase da Engenharia Web consiste na análise de requisitos e contém atividades necessárias para modelagem do problema a ser resolvido, como coleta de informações sobre todas as funcionalidades que o sistema deve prover (requisitos funcionais) e restrições de operação (requisitos não funcionais). Requisitos de um sistema são descrições dos serviços que devem ser fornecidos por esse sistema e as suas restrições operacionais~\cite{sommerville:2007}. % E por último a modelagem de análise.

São quatro os tipos de análises propostas: \textbf{de conteúdo}, que concentra informações sobre como a aplicação deve apresentar o conteúdo, \textbf{de interação}, que analisa a interação do usuário com aplicação, \textbf{funcional}, que observa como o conteúdo é afetado ou criado pela aplicação e a \textbf{de configuração}, que reúne informações da infraestrutura na qual a aplicação se encontra disponível.

Após levantadas, reunidas e organizadas as informações necessárias na modelagem de análise, a Engenharia Web propõe elaborar o modelo de projeto. Com as informações obtidas anteriormente no modelo de análise, o projeto é organizado em seis tópicos principais: projeto de conteúdo, projeto arquitetural, projeto de estética, projeto de navegação, projeto de interface e projeto de componentes~\cite{pressman:2005}. Existem alguns métodos que podem ser aplicados no desenvolvimento de aplicações Web, como WAE~\cite{conallem:2002}, OOWS~\cite{fons-et-al:2003}, OOHDM~\cite{schwabe-rossi:1998} e o próprio FrameWeb (vide Seção~\ref{sec-frameweb}).

Em seguida vem a fase de implementação, na qual o sistema é construído. Utiliza-se uma linguagem de programação de acordo com os requisitos dos sistema e características necessárias de operação da aplicação, como escalabilidade, tempo de resposta com servidor, organização dos dados, etc. Por último temos a fase de testes, responsável por garantir que a aplicação esteja cumprindo os requisitos levantados. Tais testes garantem conteúdo correto, navegabilidade, segurança, carga, eficiência e interoperabilidade (diversos navegadores Web). 
Como exemplo dessa abordagem temos a metodologia proposta por \citeonline{conallem:2002}, que descreve um processo de desenvolvimento de software iterativo, incremental e centrado em casos de uso. As atividades do processo são mostradas na Figura~\ref{fig-processo-conallem-2002}. O sub-processo ``Reiterar'' inicia com a revisão e refinamento do plano da iteração. A as atividades são executadas em paralelo: levantamento de requisitos, análise, projeto da arquitetura, projeto detalhado, implementação, testes, gerência de alterações, desenvolvimento do ambiente e desenvolvimento do projeto. Após uma iteração, apresentam-se os modelos construídos aos \textit{stakeholders}.

\begin{figure}
\centering 
\includegraphics[]{../pg-monografia/figuras/Processo-Conallem-2002.jpg}
\caption{Processo de desenvolvimento de Software proposto por~\cite{conallem:2002}.}
\label{fig-processo-conallem-2002}
\end{figure}

\section{Frameworks}
\label{subsec-frameworks}

Foi observado que a maioria dos sistemas de informação desenvolvidos para a Web (WIS) possuíam uma infraestrutura muito similar. Desenvolveram-se então \textit{frameworks} que continham generalizações do desenvolvimento desta infraestrutura. 
	
Desta forma, um \textit{framework} pode ser considerado como uma aplicação reutilizável e semi-completa que pode ser especializada para produzir aplicações personalizadas~\cite{husted-et-al:2004}. O uso de \textit{frameworks} permite a construção da aplicação sem muito esforço de codificação, pois utiliza-se seu código em conjunto com o código da aplicação, criando um código necessário para atender tal funcionalidade.

Um exemplo é a autenticação de usuário, presente em várias aplicações. Alguns \textit{frameworks} já disponibilizam esse código de forma independente das demais partes do sistema, permitindo assim a personalização para a aplicação em construção. \citeonline{SOUZA:2007} organiza a maioria dos \textit{frameworks} de WebApps em seis categorias diferentes:

\begin{itemize}
    \item Frameworks MVC (Controladores Frontais);
    \item Frameworks Decoradores;
    \item Frameworks de Mapeamento Objeto/Relacional;
    \item Frameworks de Injeção de Dependência (Inversão de Controle);
    \item Frameworks para Programação Orientada a Aspectos (AOP);
    \item Frameworks para Autenticação e Autorização.
\end{itemize}

Nas próximas subseções são descritas as categorias de \textit{frameworks} MVC (Controlador Frontal), Mapeamento Objeto/Relacional e de Injeção de Dependência. Em seguida, apresentamos os \textit{frameworks} utilizados neste trabalho, a saber: CodeIgniter e NodeJS.


\subsection{Frameworks MVC}
\label{subsec-frameworks-mvc}
Segundo \citeonline{SOUZA:2007}, o MVC (\textit{Model-View-Controller}) ou Modelo-Visão-Controla\-dor é um arquitetura baseada em camadas desenvolvida pelo Centro de Pesquisas da Xerox de Palo Alto (Xerox PARC) para a linguagem Smalltalk em 1979~\cite{Reenskaug:1979}.

Com objetivo de favorecer a manutenibilidade da aplicação, o \textit{framework} MVC faz uma separação distinta entre Dados (\textit{Model}) e o \textit{Layout} (\textit{View}). Assim, em caso de mudanças necessárias no \textit{layout} da aplicação, estas não impliquem em mudanças na camada de dados. As funções de busca e recuperação de dados continuam preservadas. Entre essas duas camadas situa-se a camada de Controle (\textit{Controller}), que é responsável pela comunicação entre elas. Nela está presente a lógica de acesso aos dados, de negócio, de apresentação e de interação com o usuário.
A Figura~\ref{fig-modelo-mvc} exemplifica a relação entre \textit{Model}, \textit{View}, \textit{Controller} e usuários.

\begin{figure}
\centering 
\includegraphics[width=.85\textwidth]{../pg-monografia/figuras/Modelo-MVC.jpg}
\caption{Diagrama representando a arquitetura MVC. Retirado do vídeo do Curso de CodeIgniter para iniciantes da RBTech.}
\label{fig-modelo-mvc}
\end{figure}

Vamos supor uma aplicação financeira que realiza cálculo de juros. É fornecida uma interface gráfica com campos para inserir valores, escolher o tipo de cálculo (juros simples ou composto) e um botão para envio dos valores para execução do cálculo. O que se vê na interface gráfica foi codificado na camada \textit{View}.

Ao clicar no botão há uma requisição com os valores digitados no formulário e o tipo de cálculo selecionado. O evento do botão é como um pedido a um intermediador que prepara as informações para então enviá-las para o cálculo. Este intermediador nós chamamos de \textit{Controller}. O controlador é o único no sistema que conhece o responsável pela execução do cálculo, neste caso a camada que contém as regras de negócios. Esta operação matemática será realizada pelo \textit{Model} assim que ele receber um pedido do \textit{Controller}.

O \textit{Model} realiza a operação matemática e retorna o valor calculado para o \textit{Controller}, que também é o único que possui conhecimento da existência da camada de visualização. Tendo o valor ``em mãos'', o intermediador o repassa para a interface gráfica que exibirá para o usuário. Caso esta operação deva ser registrada em uma base de dados, o \textit{Model} se encarrega também desta tarefa.

Porém, na plataforma Web a arquitetura MVC precisa ser levemente adaptada, visto que o \textit{Model}, situado no servidor Web, não pode notificar a \textit{View} sobre alterações, já que esta encontra-se no navegador do lado do cliente e a comunicação é sempre iniciada pelo cliente. Portanto o nome correto para esse padrão arquitetônico, quando aplicado à Web, seria ``Controlador Frontal'' (\textit{Front Controller})~\cite{crupi-et-al:2003,SOUZA:2007}.
A Figura~\ref{fig-controlador-frontal} representa o funcionamento de um Controlador Frontal na Web.

\begin{figure}
\centering 
\includegraphics[width=.85\textwidth]{../pg-monografia/figuras/Controlador-Frontal.jpg}
\caption{ Representação de um Controlador Frontal na Web~\cite{SOUZA:2007}.}
\label{fig-controlador-frontal}
\end{figure}

\subsection{Frameworks de Mapeamento Objeto/Relacional}
\label{subsec-frameworks-map-obj-rel}
Atualmente a maioria das aplicações usa o modelo de banco de dados relacional, porém grande parte da implementação é orientada a objetos, por ser um paradigma que permite melhor manutenibilidade e reuso do código. Temos então uma ``incompatibilidade de paradigmas'' (\textit{paradigm mismatch})~\cite{SOUZA:2007}.

Isso se dá porque ``operações de SQL [linguagem estruturada de consultas para SGBDRs] como projeção sempre unem resultados em uma representação em tabelas de dados resultantes. Isso é bem diferente do grafo de objetos interconectados usados para executar uma lógica de negócio em um aplicativo Java!''~\cite{bauer2005hibernate}.

Surge então, na década de 80, o Mapeamento Objeto/Relacional (ORM -- \textit{Object Relational Mapping}), que consiste na representação das tabelas por meio de classes e os registros de cada tabela são representados como instâncias das classes correspondentes. Dessa forma o programador não precisa se preocupar com os comandos de manipulação dos dados, pois estará presente uma interface que dispõe de métodos para manipulação dos dados. É preciso apenas que o programador informe ao \textit{framework} utilizado como mapear esses objetos em forma de tabelas. Sendo assim, o programador não precisa realizar o mapeamento de forma manual, o que tomaria muito tempo e poderia gerar inconsistências no banco de dados.

\subsection{Frameworks de Injeção de Dependências}
\label{subsec-frameworks-inj-dep}

O objetivo desses \textit{frameworks} é prover baixo acoplamento entre as classes, tirando a responsabilidade de algumas classes criarem instâncias de classes das quais elas dependem para realizarem suas funções. O baixo acoplamento melhora a manutenibilidade do código. O conceito de Injeção de Dependências (DI - \textit{Dependency Injection}) diz que a responsabilidade de configurar dependências entre classes é assumida por um terceiro (no caso, o \textit{framework}). A Figura~\ref{fig-injecao-dependencia} representa o funcionamento de um \textit{framework} de Injeção de Dependências.

\begin{figure}
\centering 
\includegraphics[width=.85\textwidth]{../pg-monografia/figuras/Injecao-Dependencia.jpg}
\caption{ Funcionamento de um framework de Injeção de Dependências~\cite{SOUZA:2007}.}
\label{fig-injecao-dependencia}
\end{figure}


\subsection{CodeIgniter}
\label{subsec-codeigniter}
O CodeIgniter é um \textit{framework} de desenvolvimento ágil de aplicações em PHP. Por padrão a organização arquitetural é MVC. Seu objetivo principal é possibilitar que o desenvolvedor desenvolva projetos mais rapidamente do que se estivesse codificando do zero. Ele contém algumas funções nativas que ajudam o desenvolvimento e legibilidade do código, por exemplo:
\begin{itemize}
	\item \textit{Helpers}: são como grupo de funções que podem ser utilizadas e chamadas em qualquer parte do código. Você também pode criar um \textit{helper} e ele fica disponível para ser chamado em qualquer parte do sistema;
	\item Funções de banco de dados: \textit{queries} relacionadas a CRUD (acrônimo para \textit{Create}, \textit{Retrieve}, \textit{Update} e \textit{Delete}, operações básicas de cadastro), podem ser feitas por comandos nativos. O CodeIgniter fornece comandos como: \textit{insert}, \textit{update}, \textit{delete}. Essas funções são ótimas para o desenvolvimento do Model;
	\item Estrutura fácil: o \textit{framework} apresenta uma boa organização de pastas e arquivos. Por padrão já existem pastas chamadas \textit{Models}, \textit{Views} e \textit{Controllers}. Há uma pasta separada só para comunicação com o banco de dados. Esta estrutura pode ser observada na Figura~\ref{fig-estrutura-codeigniter}.
\end{itemize}

\begin{figure}
	\centering 
	\includegraphics[width=.5\textwidth]{../pg-monografia/figuras/Estrutura-CodeIgniter.png}
	\caption{Estrutura básica do CodeIgniter.}
	\label{fig-estrutura-codeigniter}
\end{figure}

\subsection{NodeJS}
\label{subsec-nodejs}
O NodeJS é um interpretador assíncrono de código JavaScript construído a partir do motor do navegador Chrome. É conhecido por ser rápido, pois foi construído focado em migrar a programação do JavaScript do cliente (\textit{front-end}) para os servidores, criando aplicações de alta escalabilidade (como um servidor Web), manipulando milhares de conexões/eventos simultâneas em tempo real numa única máquina física. Logo a linguagem utilizada para \textit{front-end} e \textit{back-end} se torna a mesma, o JavaScript.

A instalação do NodeJS vem acompanhada do NPM. O NPM é um gerenciador de pacotes para adição de novas funcionalidades e reúso de código.

Neste trabalho utilizou-se o EJS que é uma \textit{view engine} para escrever código JavaScript e o resultado é um código estático HTML. Assim, a aplicação se torna mais dinâmica.



\section{O Método FrameWeb}
\label{sec-frameweb}
FrameWeb~\cite{SOUZA:2007} é um método, baseado em \textit{frameworks}, para o desenvolvimento de sistemas de informação Web (\textit{Web Information Systems} – WISs). Mesmo existindo vários \textit{frameworks} para o desenvolvimento de aplicações, não havia nada que apresentasse e reunisse as características desses \textit{frameworks} para facilitar o desenvolvimento das aplicações na Engenharia Web (WebE).

Atualmente, o método FrameWeb concentra suas propostas na fase de projeto arquitetural, deixando desenvolvedores e organizações livres para adotar as técnicas que considerarem mais apropriadas para as demais fases do processo. O método propõe que a arquitetura de um WIS seja dividida em três camadas, podendo ser observado na Figura~\ref{fig-arquitetura-padrao-wis}:

\begin{figure}
	\centering 
	\includegraphics[width=.85\textwidth]{../pg-monografia/figuras/Arquitetura-Padrao-WIS.jpg}
	\caption{ Arquitetura padrão para WIS baseada no padrão arquitetônico Service Layer~\cite{fowler2002eea}.}
	\label{fig-arquitetura-padrao-wis}
\end{figure}

\begin{itemize}
   \item Lógica de Apresentação: provê as interfaces gráficas para a comunicação da WebApp com o usuário, apresentando o conteúdo das classes de domínio, permitindo ao usuário fazer requisições e visualizar respostas. Esta camada contém os pacotes de Visão e Controle:
   \begin{itemize}
      \item Visão: contém as páginas Web, imagens e scripts que são executados do lado do cliente e outros arquivos que relacionados à exibição de informações para o usuário. Os elementos presentes no pacote de Visão enviam estímulos causados pelo usuário (clique de um botão, preenchimento de um campo texto, acionamento de uma caixa de seleção) e esses estímulos devem ser devidamente tratados e respondidos pelas classes presentes no pacote Controle;
      \item Controle: contém classes controladoras, que são responsáveis por tratar a interação do usuário com os elementos presentes no pacote de Visão, tendo assim uma relação mútua. O pacote Controle também possui uma interdependência com o pacote Aplicação, que provê ao usuário acesso às principais funcionalidades do sistema.
   \end{itemize}
   \item Lógica de Negócio: provê as regras de negócios e funcionalidades relacionadas. Contém os pacotes de Domínio e Aplicação:
   \begin{itemize}
      \item Domínio: contém classes identificadas na fase de análise como sendo conceitos do domínio do problema;
      \item Aplicação: contém a implementação dos casos de uso identificados na fase de análise de requisitos, provendo uma camada de serviços que deve ser independente da interface com o usuário. O pacote Aplicação possui duas dependências, uma com o pacote de Domínio, pois manipula objetos desse pacote e a outra com o pacote de Persistência, para que se possa recuperar, gravar, alterar e excluir objetos de domínio de acordo com a execução dos casos de uso.
   \end{itemize}
   \item Lógica de Acesso a Dados: camada que provê acesso aos dados para o domínio da aplicação. Ela possui apenas o pacote de Persistência:
   \begin{itemize}
      \item Persistência: contém classes que gravam as informações de domínio que precisam ser persistidas pelo sistema. O FrameWeb recomenda o uso do padrão de projeto \textit{Data Access Object} (DAO)~\cite{Alur2003}, que separa a tecnologia de persistência de dados da Lógica de Negócio, criando uma camada de abstração, tornando a aplicação independente do \textit{framework} ORM~\cite{prado-pg15}.
   \end{itemize}
\end{itemize}

O FrameWeb segue a mesma abordagem de linguagens de modelagem, como WAE~\cite{conallem:2002} e UWE~\cite{koch-et-al:2000}, estendendo o metamodelo UML~\cite{martins-msc16} para representar componentes mais utilizados da Web e os componentes relacionados aos \textit{frameworks}. Possui quatro tipos de diagramas, com objetivo de facilitar a implementação das camadas explicadas na seção anterior~\cite{SOUZA:2007,martins-msc16}:

\begin{itemize}
    \item Modelo de Entidades: baseado no diagrama de classes geralmente criado na fase de análise, especifica os tipos de dados e o mapeamento das classes de domínio que serão persistidas;
    \item Modelo de Persistência: ajuda na implementação das classes DAO do sistema, mostrando todas as interfaces e suas implementações juntamente com os métodos de cada uma;
    \item Modelo de Navegação: é um diagrama de classes que representa as interações das páginas Web. Demonstra o funcionamento da camada Lógica de Apresentação;
    \item Modelo de Aplicação: representa as classes do pacote Aplicação e demonstra suas dependências.
\end{itemize}

Apenas os modelos de Navegação e Persistência estão presentes no desenvolvimento do SCAP realizado neste trabalho, conforme será apresentado no Capítulo~\ref{sec-projeto}. No capítulo seguinte, apresentamos os requisitos do sistema. %Foi utilizado o \textit{framework} Codeigniter juntamente com Controlador Frontal 
