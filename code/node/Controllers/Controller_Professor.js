const express = require('express');
const Model_Professor = require('../Models/Model_Professor');
require('dotenv').config({ silent: true });

const Controller_Professor = new express.Router();

Controller_Professor.get('/Controller_Professor', async (req, res) => {

    let base_url = 'http://'+req.hostname+':'+process.env.PORT;
    let formProfessorAlert = "";
    let formRelativeAlert = "";
    let allProfessors = await Model_Professor.getAllProfessors();
    let allRelatives = await Model_Professor.getAllRelatives();
    let allTypeRelationship = await Model_Professor.getAllTypeRelationship();

    res.render('View_Professor', {
        base_url,
        title : 'Controller_Professor',
        allProfessors,
        allRelatives,
        formProfessorAlert,
        formRelativeAlert,
        allTypeRelationship
    });

    //res.send('Ok');
});

Controller_Professor.post('/Controller_Professor/insertProfessor', async (req, res) => {

    let base_url = 'http://'+req.hostname+':'+process.env.PORT;
    let formProfessorAlert = "";
    let formRelativeAlert = "";

    let nome = req.body.nome.trim();
    let sobrenome = req.body.sobrenome.trim();
    let email = req.body.email.trim();
    let telefone = req.body.telefone.trim();

    if(nome == ""){
        formProfessorAlert += "Nome requerido. ";
    }

    if(sobrenome == ""){
        formProfessorAlert += "Sobrenome requerido. ";
    }

    if(email == ""){
        formProfessorAlert += "Email requerido. "; 
    }

    if(!email.includes("@")){
        formProfessorAlert += "Email inválido. ";   
    }

    if(telefone == ""){
        formProfessorAlert += "Telefone requerido. ";
    }

    if(formProfessorAlert == ""){
        await Model_Professor.insertProfessor(req);   
        formProfessorAlert = "Dados Validados com Sucesso.";
    }

    let allProfessors = await Model_Professor.getAllProfessors();
    let allRelatives = await Model_Professor.getAllRelatives();
    let allTypeRelationship = await Model_Professor.getAllTypeRelationship();

    res.render('View_Professor', {
        base_url,
        title : 'Controller_Professor',
        allProfessors,
        allRelatives,
        formProfessorAlert,
        formRelativeAlert,
        allTypeRelationship
    });
});

Controller_Professor.get('/Controller_Professor/deleteProfessor/:id', async (req, res) => {
    
    let base_url = 'http://'+req.hostname+':'+process.env.PORT;
    let formProfessorAlert = "";
    let formRelativeAlert = "";
    
    if(await Model_Professor.deleteProfessor(req.params.id) == 1){
        formProfessorAlert = "Professor com Parentesco Cadastrado. Impossível Remover.";
    }else{
        formProfessorAlert = "Professor removido com Sucesso.";
    }
    
    let allProfessors = await Model_Professor.getAllProfessors();
    let allRelatives = await Model_Professor.getAllRelatives();
    let allTypeRelationship = await Model_Professor.getAllTypeRelationship();

    res.render('View_Professor', {
        base_url,
        title : 'Controller_Professor',
        allProfessors,
        allRelatives,
        formProfessorAlert,
        formRelativeAlert,
        allTypeRelationship
    });
});

Controller_Professor.get('/Controller_Professor/editProfessor/:id', async (req, res) => {
    
    let base_url = 'http://'+req.hostname+':'+process.env.PORT;
    let professor = await Model_Professor.getProfessor(req.params.id);
    
    res.render('View_Edit_Professor', {
        base_url,
        title : 'Controller_Professor',
        professor
    });
});

Controller_Professor.post('/Controller_Professor/updateProfessor/:id', async (req, res) =>{

    await Model_Professor.updateProfessor(req);
    res.redirect('/Controller_Professor');
});

// public function insertRelative(){
//     $data['title'] = 'Cadastro de Professor';
//     $data['formRelativeError'] = NULL;

//     //die( empty($_POST['id_professor1']) );

//     if( empty($_POST['id_professor1']) == 1){
//         $data['formRelativeError'] = 'Campo Professor 1 está vazio.';
//     }else if( empty($_POST['id_professor2']) == 1){
//         $data['formRelativeError'] = 'Campo Professor 2 está vazio.';
//     }else if($_POST['id_professor1'] == $_POST['id_professor2']){
//         $data['formRelativeError'] = 'Um professor não pode ser parente de si mesmo.';
//     }

//     if($data['formRelativeError'] == NULL){
//         $this->Model_Professor->insertRelative();
//         $data['formRelativeError'] = 'Parentesco Cadastrado com Sucesso.';
//     }

//     $data['formError'] = NULL;
//     $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
//     $data['allRelatives'] = $this->Model_Professor->getAllRelatives();
//     $data['allTypeRelationship'] = $this->Model_Professor->getAllTypeRelationship();
//     $this->load->view('View_Professor.php', $data);
// }

Controller_Professor.post('/Controller_Professor/insertRelative', async (req, res) =>{
    let base_url = 'http://'+req.hostname+':'+process.env.PORT;
    let formProfessorAlert = "";
    let formRelativeAlert = "";
    
    if(req.body.id_professor1 == ''){
        formRelativeAlert = "Campo professor 1 está vazio.";
    }else if(req.body.id_professor2 == ''){
        formRelativeAlert = "Campo professor 2 está vazio.";
    }else if(req.body.id_professor1 == req.body.id_professor2){
        formRelativeAlert = "Um professor não pode ser parente de si mesmo.";
    }

    if(formRelativeAlert == ""){
        await Model_Professor.insertRelative(req);
        formRelativeAlert = "Parentesco Cadastrado com Sucesso.";
    }
       
    let allProfessors = await Model_Professor.getAllProfessors();
    let allRelatives = await Model_Professor.getAllRelatives();
    let allTypeRelationship = await Model_Professor.getAllTypeRelationship();
    
    res.render('View_Professor', {
        base_url,
        title : 'Controller_Professor',
        allProfessors,
        allRelatives,
        formProfessorAlert,
        formRelativeAlert,
        allTypeRelationship
    });
});

Controller_Professor.get('/Controller_Professor/deleteRelative/:relationship', async (req, res) =>{
    let splittedRelationship = req.params.relationship.split("-");
    await Model_Professor.deleteRelative(splittedRelationship[0], splittedRelationship[2]);
    res.redirect('/Controller_Professor');
});




module.exports = Controller_Professor;