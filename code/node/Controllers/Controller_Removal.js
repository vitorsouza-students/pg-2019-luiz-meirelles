const express = require('express');
const Model_Removal = require('../Models/Model_Removal');
const Model_Professor = require('../Models/Model_Professor');
var moment = require('moment');
require('dotenv').config({ silent: true });

const Controller_Removal = new express.Router();

Controller_Removal.get('/Controller_Removal', async (req, res) => {

    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;
    let formRemovalAlert = "";

    let allRemovals = await Model_Removal.getAllRemovals();
    let allTeachers = await Model_Professor.getAllProfessors();
    let allRequests = await Model_Removal.getAllRequests();
    let allRapporteurs = await Model_Removal.getAllRapporteurs();
    let formRequestAlert = "";
    let formRapporteursAlert = "";

    res.render('View_Removal', {
        base_url,
        title: 'Controller_Removal',
        allRemovals,
        allRequests,
        allTeachers,
        allRapporteurs,
        formRemovalAlert,
        formRequestAlert,
        formRapporteursAlert
    });

    //res.send('Ok');
});

Controller_Removal.get('/Controller_Removal/deleteRemoval/:id', async (req, res) => {

    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;
    let formRemovalAlert = "";

    let id = req.params.id;
    let result = await Model_Removal.deleteRemoval(id);

    if (result == 1) {
        formRemovalAlert = 'Afastamento solicitado. Impossível Remover.';
    } else if (result == 2) {
        formRemovalAlert = 'Afastamento com relator. Impossível Remover.';
    } else {
        formRemovalAlert = 'Afastamento removido com Sucesso.';
    }

    let allRemovals = await Model_Removal.getAllRemovals();
    let allRequests = await Model_Removal.getAllRequests();
    let allTeachers = await Model_Professor.getAllProfessors();
    let allRapporteurs = await Model_Removal.getAllRapporteurs();
    let formRequestAlert = "";
    let formRapporteursAlert = "";

    res.render('View_Removal', {
        base_url,
        title: 'Controller_Removal',
        allRemovals,
        allRequests,
        allTeachers,
        allRapporteurs,
        formRemovalAlert,
        formRequestAlert,
        formRapporteursAlert
    });

});

Controller_Removal.post('/Controller_Removal/insertRemoval', async (req, res) => {

    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;
    let formRemovalAlert = "";
    


        if(req.body.data_solicitacao == ""){
            formRemovalAlert = "Data Solicitação Requerida."; 
        }
    
        if(req.body.data_inicio_afastamento == ""){
            formRemovalAlert = formRemovalAlert + " Data Ínicio Afastamento Requerida."; 
        }
    
        if(req.body.data_fim_afastamento == ""){
            formRemovalAlert = formRemovalAlert + " Data Fim Afastamento Requerida."; 
        }
    
        if(req.body.motivo == ""){
            formRemovalAlert = formRemovalAlert + " Motivo Rquerido."; 
        }
    
        if(req.body.data_inicio_evento == ""){
            formRemovalAlert = formRemovalAlert + " Data Ínicio Evento Requerida."; 
        }
    
        if(req.body.data_fim_evento == ""){
            formRemovalAlert = formRemovalAlert + " Data Fim Evento Requerida."; 
        }
    
        if(req.body.nome_evento == ""){
            formRemovalAlert = formRemovalAlert + " Nome Evento Requerido."; 
        }
    
        if(req.body.data_inicio_afastamento > req.body.data_fim_afastamento){
            formRemovalAlert = formRemovalAlert + "Data do Ínicio do Afastamento não pode ser menor que a data Final.";
        }
    
        if(formRemovalAlert == ""){
            await Model_Removal.insertRemoval(req);
            formRemovalAlert = "Afastamento Cadastrado com sucesso.";
        }
        
    let allRemovals = await Model_Removal.getAllRemovals();
    let allRequests = await Model_Removal.getAllRequests();
    let allTeachers = await Model_Professor.getAllProfessors();
    let allRapporteurs = await Model_Removal.getAllRapporteurs();
    let formRequestAlert = "";
    let formRapporteursAlert = "";

    res.render('View_Removal', {
        base_url,
        title: 'Controller_Removal',
        allRemovals,
        allRequests,
        allTeachers,
        allRapporteurs,
        formRemovalAlert,
        formRequestAlert,
        formRapporteursAlert
    });
});

Controller_Removal.get('/Controller_Removal/editRemoval/:id', async (req, res) => {

    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;

    let removal = await Model_Removal.getRemoval(req.params.id);
    res.render('View_Edit_Removal', {
        base_url,
        moment,
        removal
    });
});

Controller_Removal.post('/Controller_Removal/updateRemoval/:id', async (req, res) => {
        
    await Model_Removal.updateRemoval(req);
    
    res.redirect('/Controller_Removal');
});

Controller_Removal.get('/Controller_Removal/deleteRequest/:id', async (req, res) => {
        
    await Model_Removal.deleteRequest(req.params.id);
    
    res.redirect('/Controller_Removal');
});


Controller_Removal.post('/Controller_Removal/insertRequest', async (req, res) => {

    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;
    let formRequestAlert = "";
    let field_id_teacher = req.body.field_id_teacher.split(" ");

    let reqExists = await Model_Removal.checkRequestExists(field_id_teacher[0], req.body.field_id_removal);
    let rmvalRequested = await Model_Removal.checkRemovalRequested(req.body.field_id_removal);
    
    // console.log("field_id_teacher: ",req.body.field_id_teacher[0]);
    // console.log("field_id_removal: ",req.body.field_id_removal);
    // console.log("reqExists: ", reqExists);
   
    if(field_id_teacher.length == 0){
        // console.log("1");
        formRequestlAlert = "Não há nenhum Professor cadastrado."; 
    }else if(req.body.field_id_removal.length == 0){
        // console.log("2");
        formRequestAlert = "Não há nenhum Afastamento cadastrado.";
    } else if(reqExists[0].requestExists == 1){
        // console.log("3");
        formRequestAlert = "Professor com Afastamento já cadastrado.";
    } else if(rmvalRequested[0].removalRequested == 1){
        // console.log("4");
        formRequestAlert = "Afastamento já solicitado.";
    }else{
        // console.log("5");
        await Model_Removal.insertRequest(field_id_teacher[0], req.body.field_id_removal);
        formRequestAlert = "Inserido com Sucesso";
    }

    formRemovalAlert = "";
        
    let allRemovals = await Model_Removal.getAllRemovals();
    let allRequests = await Model_Removal.getAllRequests();
    let allTeachers = await Model_Professor.getAllProfessors();
    let allRapporteurs = await Model_Removal.getAllRapporteurs();
    let formRapporteursAlert = "";

    res.render('View_Removal', {
        base_url,
        title: 'Controller_Removal',
        allRemovals,
        allRequests,
        allTeachers,
        allRapporteurs,
        formRemovalAlert,
        formRequestAlert,
        formRapporteursAlert
    });
});

Controller_Removal.get('/Controller_Removal/deleteRapporteur/:id', async (req, res) => {
        
    await Model_Removal.deleteRapporteur(req.params.id);
    
    res.redirect('/Controller_Removal');
});

Controller_Removal.post('/Controller_Removal/insertRapporteurs', async (req, res) => {
    let base_url = 'http://' + req.hostname + ':' + process.env.PORT;
    let formRapporteursAlert = "";
    let field_id_teacher = req.body.field_id_teacher.split(" ");
    let relExists = await Model_Removal.checkRelative(field_id_teacher[0], req.body.field_id_removal);
    let reqOwner = await Model_Removal.checkRequestOwner(field_id_teacher[0], req.body.field_id_removal);
    let rapExists = await Model_Removal.checkRapporteursExists(req.body.field_id_removal);

    // console.log("relExists", relExists);
    // console.log("reqOwner", reqOwner);
    // console.log("rapExists", rapExists);

    if(field_id_teacher.length == 0){
        // console.log("1");
        formRapporteursAlert = "Não há nenhum Professor cadastrado."; 
    }else if(req.body.field_id_removal.length == 0){
        // console.log("2");
        formRapporteursAlert = "Não há nenhum Afastamento cadastrado.";
    } else if(relExists[0].relativeExists == 1){
        // console.log("3");
        formRapporteursAlert = "Professor com Parentesco ao Solicitante. Não pode ser relator.";
    } else if( reqOwner[0].rapporteursOwner == 1){
        // console.log("4");
        formRapporteursAlert = "Um professor não pode Relator do seu próprio Afastamento.";
    }else if( rapExists.length != 0 ){
        formRapporteursAlert = "Já há relator para esse afastamento."
    }else{
        // console.log("5");
        await Model_Removal.insertRapporteurs(field_id_teacher[0], req.body.field_id_removal);
        formRapporteursAlert = "Inserido com Sucesso";
    }

    formRemovalAlert = "";
    formRequestAlert = "";
    let allRemovals = await Model_Removal.getAllRemovals();
    let allRequests = await Model_Removal.getAllRequests();
    let allTeachers = await Model_Professor.getAllProfessors();
    let allRapporteurs = await Model_Removal.getAllRapporteurs();

    res.render('View_Removal', {
        base_url,
        title: 'Controller_Removal',
        allRemovals,
        allRequests,
        allTeachers,
        allRapporteurs,
        formRemovalAlert,
        formRequestAlert,
        formRapporteursAlert
    });
});

module.exports = Controller_Removal;