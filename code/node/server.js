'use strict';

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
require('dotenv').config({ silent: true });
require('./database').DBConnect();
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Controllers
app.use(require('./Controllers/Controller_Professor'));
app.use(require('./Controllers/Controller_Removal'));

//Set view engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'Views'));

var port = process.env.PORT;

app.listen(port, function(){
    console.log("App is running on port " + port);
});