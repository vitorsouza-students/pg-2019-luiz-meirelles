const mysql = require('mysql');
const moment = require('moment');
require('dotenv').config({ silent: true });

let conector = mysql.createConnection({
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_DATABASE
});

function DBConnect() {
    conector.connect(function (err) {
        if (err) {
          console.log('--------- ERRO AO CONECTAR NO BANCO DE DADOS ' + moment().format('DD/MM/YYYY HH:MM:SS') + ' ---------');
          console.log(err);
          DBConnect();
        } else {
          console.log('--------- BANCO DE DADOS CONECTADO COM SUCESSO ' + moment().format('DD/MM/YYYY HH:MM:SS') + ' ---------');
        }
      });

      conector.on('error',function(error){
        console.log(error.code);
          //Para caso perca a conexão com o Banco ou para caso tente fazer uma query e já perdeu a conexão.
          if(error.code == 'PROTOCOL_CONNECTION_LOST' || error.code == 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR'){
            console.log("--- Tentando Reconectar no Bando de Dados. " + moment().format('DD/MM/YYYY HH:MM:SS') + " ---");
            DBConnect();  
          }
      });
}

module.exports = {
    DBConnect: DBConnect,
    conector
}
