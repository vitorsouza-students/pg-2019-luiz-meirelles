const { conector } = require('../database');

function getAllProfessors() {
  const query = `SELECT * FROM professor;`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function getProfessor(id){
  const query = `SELECT * FROM professor WHERE id = ?;`;
  let info = [id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if(err) reject(err);
      return resolve(result);
    });
  });
}

function insertProfessor(req){
  const query = `INSERT INTO professor (nome, sobrenome, email, telefone) VALUES(?, ?, ?, ?);`;
  let info = [req.body.nome, req.body.sobrenome, req.body.email, req.body.telefone];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err,result) => {
      if(err) reject(err);
      resolve(result);
    });
  });
}

function deleteProfessor(id) {
  let error = null;

  //Checking if exists Professor with relative.
  const query = `SELECT * FROM parentesco WHERE id_professor1 = ${id} OR id_professor2 = ${id};`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if (err) reject(err);
      
      if(result.length != 0){
        error = 1;
        return resolve(error);
      }
    });
    
    const query2 = `DELETE FROM professor where id = ${id}`;
    conector.query(query2, (err, result) => {
      if (err) reject(err);
      return resolve(error);
    });

  });
}

function updateProfessor(req){
  const query = `UPDATE professor SET nome = ?, sobrenome = ?, email = ?, telefone = ? WHERE id = ?;`;
  let info = [req.body.nome, req.body.sobrenome, req.body.email, req.body.telefone, req.params.id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err,result) => {
      if(err) reject(err);
      resolve(result);
    });
  });
}

function getAllRelatives(){
  const query = `SELECT p1.id AS id_professor1, p1.nome AS nome_professor1, p1.sobrenome AS sobrenome_professor1, tp.tipo_parentesco, tp.id, p2.id AS id_professor2, p2.nome AS nome_professor2, p2.sobrenome AS sobrenome_professor2
         FROM professor AS p1 INNER JOIN parentesco AS parentesco1 ON p1.id = parentesco1.id_professor1
         INNER JOIN tipo_parentesco AS tp ON parentesco1.tipo_parentesco = tp.id 
         INNER JOIN professor AS p2 ON parentesco1.id_professor2 = p2.id
         ORDER BY p1.id;`;
  
  return new Promise((resolve, reject) => {
  conector.query(query, (err,result) => {
    if(err) reject(err);
      resolve(result);
    });
  });
}

function deleteRelative(id_professor1, id_professor2){

  const query1 = `DELETE FROM parentesco WHERE id_professor1 = ? AND id_professor2 = ?`;
  let info1 = [id_professor1, id_professor2];

  const query2 = `DELETE FROM parentesco WHERE id_professor1 = ? AND id_professor2 = ?`;
  let info2 = [id_professor2, id_professor1];
  
  return new Promise((resolve, reject) => {
    conector.query(query1, info1, (err,result1) => {
      if(err) reject(err);
      conector.query(query2, info2, (err,result2) => {
        if(err) reject(err);
        resolve(result1 + " " + result2);
      });
    });   
    }
  );
}

function getAllTypeRelationship(){
  const query = `SELECT tipo_parentesco FROM tipo_parentesco;`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if(err) reject(err);
      resolve(result);
    });
  });
}

// function insertRelative(){
//   // Get id of tipo_parentesco in BD first.
//   $tipo_parentesco = $this->input->post('tipo_parentesco');
//   $query = $this->db->query('SELECT id FROM tipo_parentesco WHERE tipo_parentesco = ' . "'$tipo_parentesco '");
//   $id_tipo_parentesco = $query->result();

//   //If type of relationship are brothers or couple:
//   if($id_tipo_parentesco[0]->id == 1 || $id_tipo_parentesco[0]->id == 2){

//       $data1 = array(
//           'id_professor1' => $this->input->post('id_professor1'),
//           'tipo_parentesco' => $id_tipo_parentesco[0]->id,
//           'id_professor2' => $this->input->post('id_professor2'),
//       );
//       $this->db->insert('parentesco', $data1);

//       $data2 = array(
//           'id_professor1' => $this->input->post('id_professor2'),
//           'tipo_parentesco' => $id_tipo_parentesco[0]->id,
//           'id_professor2' => $this->input->post('id_professor1'),
//       );
//       $this->db->insert('parentesco', $data2);
//   }

//   //If type of relationship is father:
//   if($id_tipo_parentesco[0]->id == 3){
//       $data1 = array(
//           'id_professor1' => $this->input->post('id_professor1'),
//           'tipo_parentesco' => $id_tipo_parentesco[0]->id,
//           'id_professor2' => $this->input->post('id_professor2'),
//       );
//       $this->db->insert('parentesco', $data1);
      
//       //Then adds id_professor2 as son of id_professor1.
//       $data2 = array(
//           'id_professor1' => $this->input->post('id_professor2'),
//           'tipo_parentesco' => 4,
//           'id_professor2' => $this->input->post('id_professor1'),
//       );
//       $this->db->insert('parentesco', $data2);
//   }

//   //If type of relationship is son:
//   if($id_tipo_parentesco[0]->id == 4){
//       $data1 = array(
//           'id_professor1' => $this->input->post('id_professor1'),
//           'tipo_parentesco' => $id_tipo_parentesco[0]->id,
//           'id_professor2' => $this->input->post('id_professor2'),
//       );
//       $this->db->insert('parentesco', $data1);
      
//       //Then adds id_professor2 as father of id_professor1.
//       $data2 = array(
//           'id_professor1' => $this->input->post('id_professor2'),
//           'tipo_parentesco' => 3,
//           'id_professor2' => $this->input->post('id_professor1'),
//       );
//       $this->db->insert('parentesco', $data2);
//   }

// }

function insertRelative(req){
  let IDrelationshipType;
  
  // Get the id of tipo_parentesco in BD first.
  const query1 = `SELECT id FROM tipo_parentesco WHERE tipo_parentesco = ? ;`; 
  let info1 = [req.body.tipo_parentesco];

  return new Promise((resolve, reject) => {
    conector.query(query1, info1, (err, result1) => {
      if(err) reject(err);
      IDrelationshipType = result1[0].id;

      //If type of relationship are brothers or couple:
      if(IDrelationshipType == 1 || IDrelationshipType == 2){
      
        const query2 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info2 = [req.body.id_professor1, IDrelationshipType, req.body.id_professor2];
        
        conector.query(query2, info2, (err, result2) => {
          if(err) reject(err);
          
        });

        const query3 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info3 = [req.body.id_professor2, IDrelationshipType, req.body.id_professor1];
        
        conector.query(query3, info3, (err, result3) => {
          if(err) reject(err);
        }); 

      }

      //If type of relationship is father:
      if(IDrelationshipType == 3){
        const query4 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info4 = [req.body.id_professor1, IDrelationshipType, req.body.id_professor2];
        
        conector.query(query4, info4, (err, result4) => {
          if(err) reject(err);
        });
        //And adds id_professor2 as son of id_professor1.
        const query5 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info5 = [req.body.id_professor2, 4, req.body.id_professor1];
        
        conector.query(query5, info5, (err, result5) => {
          if(err) reject(err);
        }); 
      }

      //If type of relationship is son:
      if(IDrelationshipType == 4){
        const query6 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info6 = [req.body.id_professor1, IDrelationshipType, req.body.id_professor2];
        
        conector.query(query6, info6, (err, result6) => {
          if(err) reject(err);
        });
        
        //Then adds id_professor2 as father of id_professor1.
        const query7 = `INSERT INTO parentesco (id_professor1, tipo_parentesco, id_professor2) values (?, ?, ?);`;
        let info7 = [req.body.id_professor2, 3, req.body.id_professor1];
        
        conector.query(query7, info7, (err, result7) => {
          if(err) reject(err);
        }); 
      }

      resolve();
    });
  });
}

module.exports = {
  getAllProfessors,
  getProfessor,
  insertProfessor,
  deleteProfessor,
  updateProfessor,
  getAllRelatives,
  deleteRelative,
  getAllTypeRelationship,
  insertRelative
}