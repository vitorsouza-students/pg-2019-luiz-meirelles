const { conector } = require('../database');

function getAllRemovals() {
  const query = `SELECT id, 
        DATE_FORMAT(data_solicitacao, "%d/%m/%Y" ) AS data_solicitacao, 
        DATE_FORMAT(data_inicio_afastamento, "%d/%m/%Y" ) AS data_inicio_afastamento, 
        DATE_FORMAT(data_fim_afastamento, "%d/%m/%Y" ) AS data_fim_afastamento,  
        tipo_afastamento, 
        motivo_afastamento, 
        onus, 
        DATE_FORMAT(data_inicio_evento, "%d/%m/%Y") as data_inicio_evento, 
        DATE_FORMAT(data_fim_evento, "%d/%m/%Y") as data_fim_evento,
        nome_evento
        from afastamento;`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function insertRemoval(req) {
  const query = `INSERT INTO afastamento (data_solicitacao, data_inicio_afastamento, data_fim_afastamento, tipo_afastamento, motivo_afastamento, onus, data_inicio_evento, data_fim_evento, nome_evento) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);`;
  let info = [req.body.data_solicitacao, req.body.data_inicio_afastamento, req.body.data_fim_afastamento, req.body.tipo_afastamento, req.body.motivo, req.body.onus, req.body.data_inicio_evento, req.body.data_fim_evento, req.body.nome_evento];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}


function deleteRemoval(id) {
  let error = "";

  let info = [id];

  const query1 = `SELECT * FROM requests WHERE removal_id = ?;`;

  const query2 = `SELECT * FROM rapporteurs WHERE removal_id = ?;`;

  const query3 = `DELETE FROM afastamento WHERE id = ?;`;

  return new Promise((resolve, reject) => {
    conector.query(query1, info, (err, result) => {
      if (err) reject(err);
      if (result.length != 0) {
        return resolve(error = 1);
      }
    });

    conector.query(query2, info, (err, result) => {
      if (err) reject(err);
      if (result.length != 0) {
        return resolve(error = 2);
      }
    });

    conector.query(query3, info, (err, result) => {
      if (err) reject(err);
      return resolve(error);
    });

  });
}

function getRemoval(id) {
  const query = `SELECT * FROM afastamento WHERE id = ?`;
  let info = [id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function updateRemoval(req) {
  
  const query = `UPDATE afastamento SET data_solicitacao = ?, data_inicio_afastamento = ?, data_fim_afastamento = ?, tipo_afastamento = ?, motivo_afastamento = ?, onus = ?, data_inicio_evento = ?, data_fim_evento = ?, nome_evento = ? WHERE id = ?;`;
  let info = [req.body.data_solicitacao, req.body.data_inicio_afastamento, req.body.data_fim_afastamento, req.body.tipo_afastamento, req.body.motivo, req.body.onus, req.body.data_inicio_evento, req.body.data_fim_evento, req.body.nome_evento, req.params.id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function getAllRequests(){
  const query = `SELECT r.id AS requestId, p.id AS teacherId, p.nome, p.sobrenome, r.removal_id FROM requests AS r INNER JOIN professor AS p ON teacher_id = p.id`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function getAllRapporteurs(){
  const query = `SELECT r.id AS rapporteursId, p.id AS teacherId, p.nome, p.sobrenome, r.removal_id FROM rapporteurs AS r INNER JOIN professor AS p ON teacher_id = p.id;`;

  return new Promise((resolve, reject) => {
    conector.query(query, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function deleteRequest(id){
  const query = `DELETE FROM requests WHERE id = ?`;
  let info = [id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function checkRequestExists(teacher_id, removal_id){
  const query = `SELECT EXISTS (SELECT professor.id, professor.nome, professor.sobrenome, removal_id FROM scap.requests INNER JOIN professor ON teacher_id = professor.id WHERE professor.id = ? AND removal_id = ?) AS requestExists;`;
  let info = [teacher_id, removal_id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function checkRemovalRequested(removal_id){
  const query = `SELECT exists (SELECT * FROM requests WHERE removal_id=?) AS removalRequested;`;
  let info = [removal_id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function insertRequest(field_id_teacher, field_id_removal){
  const query = `INSERT INTO requests (teacher_id, removal_id) VALUES (?, ?);`;
  let info = [field_id_teacher, field_id_removal];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function deleteRapporteur(id){
  const query = `DELETE FROM rapporteurs WHERE id = ?;`;
  let info = [id];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function checkRelative(professorID, removalID){
  const query = `SELECT EXISTS (SELECT * FROM (SELECT id_professor2 FROM parentesco WHERE id_professor1 = (SELECT teacher_id FROM requests WHERE removal_id = ?)) AS p WHERE id_professor2 = ?) AS relativeExists;`;
  let info = [removalID, professorID];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

// function checkRequestOwner($teacherId, $removalId){
//   $query = $this->db->query('SELECT EXISTS (SELECT * FROM requests where removal_id = '.$removalId.' and teacher_id = '.$teacherId.') AS rapporteursOwner;');        
//   return $query->row();
// }

function checkRequestOwner(professorID, removalID){
  const query = `SELECT EXISTS (SELECT * FROM requests where removal_id = ? and teacher_id = ?) AS rapporteursOwner;`;
  let info = [removalID, professorID];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function insertRapporteurs(professorID, removalID){
  const query = `INSERT INTO rapporteurs (teacher_id, removal_id) VALUES (?, ?);`;
  let info = [professorID, removalID];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function checkRapporteursExists(removalID){
  const query = `SELECT * FROM rapporteurs WHERE removal_id = ?;`;
  let info = [removalID];

  return new Promise((resolve, reject) => {
    conector.query(query, info, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

module.exports = {
      getAllRemovals,
      insertRemoval,
      deleteRemoval,
      getRemoval,
      updateRemoval,
      getAllRequests,
      deleteRequest,
      checkRequestExists,
      checkRemovalRequested,
      insertRequest,
      getAllRapporteurs,
      deleteRapporteur,
      checkRelative,
      checkRequestOwner,
      insertRapporteurs,
      checkRapporteursExists
}