<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Professor extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    function insertTeacher(){
        $data = array(
            'nome' => $this->input->post('nome'),
            'sobrenome' => $this->input->post('sobrenome'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone')
        );
        $this->db->insert('professor', $data);
    }

    function getAllTeachers(){
        //$query = $this->db->query('SELECT * FROM professor');
        $query = $this->db->get('professor');
        return $query->result();
    }

    function getTeacher($id){
        $query = $this->db->query('SELECT * FROM professor WHERE `id` = ' .$id);
        return $query->row();
        echo $id;
    }

    function updateTeacher($id){
        $data = array(
            'nome' => $this->input->post('nome'),
            'sobrenome' => $this->input->post('sobrenome'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone')
        );

        $this->db->where('id', $id);
        $this->db->update('professor', $data);
    }

    function deleteTeacher($id){
        $error = null; 

        $query = $this->db->query('SELECT * FROM parentesco WHERE `id_professor1` = ' .$id. ' OR `id_professor2` = '.$id );
        if (sizeof($query->result()) != 0){
            $error = 1;
            return $error;
        }

        $this->db->where('id', $id);
        $this->db->delete('professor');

        return $error;
    }

    function insertRelative(){
        // Get id of tipo_parentesco in BD first.
        $tipo_parentesco = $this->input->post('tipo_parentesco');
        $query = $this->db->query('SELECT id FROM tipo_parentesco WHERE tipo_parentesco = ' . "'$tipo_parentesco '");
        $id_tipo_parentesco = $query->result();

        //If type of relationship are brothers or couple:
        if($id_tipo_parentesco[0]->id == 1 || $id_tipo_parentesco[0]->id == 2){

            $data1 = array(
                'id_professor1' => $this->input->post('id_professor1'),
                'tipo_parentesco' => $id_tipo_parentesco[0]->id,
                'id_professor2' => $this->input->post('id_professor2'),
            );
            $this->db->insert('parentesco', $data1);
    
            $data2 = array(
                'id_professor1' => $this->input->post('id_professor2'),
                'tipo_parentesco' => $id_tipo_parentesco[0]->id,
                'id_professor2' => $this->input->post('id_professor1'),
            );
            $this->db->insert('parentesco', $data2);
        }

        //If type of relationship is father:
        if($id_tipo_parentesco[0]->id == 3){
            $data1 = array(
                'id_professor1' => $this->input->post('id_professor1'),
                'tipo_parentesco' => $id_tipo_parentesco[0]->id,
                'id_professor2' => $this->input->post('id_professor2'),
            );
            $this->db->insert('parentesco', $data1);
            
            //Then adds id_professor2 as son of id_professor1.
            $data2 = array(
                'id_professor1' => $this->input->post('id_professor2'),
                'tipo_parentesco' => 4,
                'id_professor2' => $this->input->post('id_professor1'),
            );
            $this->db->insert('parentesco', $data2);
        }

        //If type of relationship is son:
        if($id_tipo_parentesco[0]->id == 4){
            $data1 = array(
                'id_professor1' => $this->input->post('id_professor1'),
                'tipo_parentesco' => $id_tipo_parentesco[0]->id,
                'id_professor2' => $this->input->post('id_professor2'),
            );
            $this->db->insert('parentesco', $data1);
            
            //Then adds id_professor2 as father of id_professor1.
            $data2 = array(
                'id_professor1' => $this->input->post('id_professor2'),
                'tipo_parentesco' => 3,
                'id_professor2' => $this->input->post('id_professor1'),
            );
            $this->db->insert('parentesco', $data2);
        }

    }

    function getAllRelatives(){
        $query = $this->db->query('SELECT p1.id AS id_professor1, p1.nome AS nome_professor1, p1.sobrenome AS sobrenome_professor1, tp.tipo_parentesco, tp.id, p2.id AS id_professor2, p2.nome AS nome_professor2, p2.sobrenome AS sobrenome_professor2
            FROM professor AS p1 INNER JOIN parentesco AS parentesco1 ON p1.id = parentesco1.id_professor1
            INNER JOIN tipo_parentesco AS tp ON parentesco1.tipo_parentesco = tp.id 
            INNER JOIN professor AS p2 ON parentesco1.id_professor2 = p2.id
            ORDER BY p1.id');
        return $query->result();
    }

    function getAllTypeRelationship(){
        $query = $this->db->query('SELECT tipo_parentesco FROM tipo_parentesco');
        return $query->result();
    }

    function deleteRelative($id_professor1, $tipo_parentesco, $id_professor2){
        //echo $id_professor1.$tipo_parentesco.$id_professor2;
        $this->db->query('DELETE FROM parentesco WHERE id_professor1 ='.$id_professor1.' AND id_professor2 ='.$id_professor2);
        $this->db->query('DELETE FROM parentesco WHERE id_professor1 ='.$id_professor2.' AND id_professor2 ='.$id_professor1);
    }
}