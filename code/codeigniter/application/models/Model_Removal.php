<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Removal extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    function getAllRemovals(){
        $query = $this->db->query('
        SELECT id, 
            DATE_FORMAT(data_solicitacao, "%d/%m/%Y" ) AS data_solicitacao, 
            DATE_FORMAT(data_inicio_afastamento, "%d/%m/%Y" ) AS data_inicio_afastamento, 
            DATE_FORMAT(data_fim_afastamento, "%d/%m/%Y" ) AS data_fim_afastamento,  
            tipo_afastamento, 
            motivo_afastamento, 
            onus, 
            DATE_FORMAT(data_inicio_evento, "%d/%m/%Y") as data_inicio_evento, 
            DATE_FORMAT(data_fim_evento, "%d/%m/%Y") as data_fim_evento,
            nome_evento
        from afastamento;
        ');
        
        return $query->result();
    }

    function insertRemoval(){
        $data = array(
            'data_solicitacao' => $this->input->post('data_solicitacao'),
            'data_inicio_afastamento' => $this->input->post('data_inicio_afastamento'),
            'data_fim_afastamento' => $this->input->post('data_fim_afastamento'),
            'tipo_afastamento' => $this->input->post('tipo_afastamento'),
            'motivo_afastamento' => $this->input->post('motivo'),
            'onus' => $this->input->post('onus'),
            'data_inicio_evento' => $this->input->post('data_inicio_evento'),
            'data_fim_evento' => $this->input->post('data_fim_evento'),
            'nome_evento' => $this->input->post('nome_evento')
        );
        $this->db->insert('afastamento', $data);
    }

    function deleteRemoval($id){
        $error = null; 

        $query = $this->db->query('SELECT * FROM requests WHERE `removal_id` = ' .$id);
        if (sizeof($query->result()) != 0){
            $error = 1;
            return $error;
        }

        $query = $this->db->query('SELECT * FROM rapporteurs WHERE `removal_id` = ' .$id);
        if (sizeof($query->result()) != 0){
            $error = 2;
            return $error;
        }      

        $this->db->where('id', $id);
        $this->db->delete('afastamento');

        return $error;
    }

    function getRemoval($id){
        $query = $this->db->query('SELECT * FROM afastamento WHERE `id` = ' .$id);
        return $query->row();
        echo $id;
    }

    function updateRemoval($id){
        $data = array(
            'data_solicitacao' => $this->input->post('data_solicitacao'),
            'data_inicio_afastamento' => $this->input->post('data_inicio_afastamento'),
            'data_fim_afastamento' => $this->input->post('data_fim_afastamento'),
            'tipo_afastamento' => $this->input->post('tipo_afastamento'),
            'motivo_afastamento' => $this->input->post('motivo'),
            'onus' => $this->input->post('onus'),
            'data_inicio_evento' => $this->input->post('data_inicio_evento'),
            'data_fim_evento' => $this->input->post('data_fim_evento'),
            'nome_evento' => $this->input->post('nome_evento')
        );

        $this->db->where('id', $id);
        $this->db->update('afastamento', $data);
    }

    function insertRequest(){
        $data = array(
            'teacher_id' => $this->input->post('field_id_teacher'),
            'removal_id' => $this->input->post('field_id_removal')
        );

        $this->db->insert('requests', $data);
    }

    function getAllRequests(){
        $query = $this->db->query('SELECT r.id AS requestId, p.id AS teacherId, p.nome, p.sobrenome, r.removal_id FROM requests AS r INNER JOIN professor AS p ON teacher_id = p.id');
        return $query->result();
    }

    function checkRequestExists($teacherId, $removalId){
        $query = $this->db->query('SELECT EXISTS (SELECT professor.id, professor.nome, professor.sobrenome, removal_id FROM scap.requests INNER JOIN professor ON teacher_id = professor.id WHERE professor.id = '.$teacherId.' AND removal_id = '.$removalId.') AS requestExists;');
        return $query->row();
    }

    function checkRemovalRequested($removalId){
        $query = $this->db->query('SELECT exists (SELECT * FROM requests WHERE removal_id='.$removalId.') AS removalRequested;');
        return $query->row();
    }

    function deleteRequest($id){
        $this->db->query('DELETE FROM requests WHERE id ='.$id);
    }

    function getAllRapporteurs(){
        $query = $this->db->query('SELECT r.id AS rapporteursId, p.id AS teacherId, p.nome, p.sobrenome, r.removal_id FROM rapporteurs AS r INNER JOIN professor AS p ON teacher_id = p.id;');
        return $query->result();
    }

    function checkRelative($teacherId, $removalId){
        $query = $this->db->query('SELECT EXISTS (SELECT * FROM (SELECT id_professor2 FROM parentesco WHERE id_professor1 = (SELECT teacher_id FROM requests WHERE removal_id = '.$removalId.')) AS p WHERE id_professor2 = '.$teacherId.') AS relativeExists;');        
        return $query->row();
    }

    function checkRequestOwner($teacherId, $removalId){
        $query = $this->db->query('SELECT EXISTS (SELECT * FROM requests where removal_id = '.$removalId.' and teacher_id = '.$teacherId.') AS rapporteursOwner;');        
        return $query->row();
    }

    function insertRapporteurs($teacherId, $removalId){
        $data = array(
            'teacher_id' => $this->input->post('field_id_teacher'),
            'removal_id' => $this->input->post('field_id_removal')
        );

        $this->db->insert('rapporteurs', $data);
    }

    function deleteRapporteur($id){
        $this->db->query('DELETE FROM rapporteurs WHERE id ='.$id);
    }

    function checkRapporteursExists($removalId){
        $query = $this->db->query('SELECT * FROM rapporteurs WHERE removal_id ='.$removalId.';');
        return $query->result();
    }
}