<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parentesco {
    private $parentesco;

    public function __construct($parentesco){
        $this->parentesco = $parentesco;
    }
    
    public function getParentesco(){
        return $this->parentesco;
    }
    
    public function setParentesco($parentesco){
        $this->parentesco = $parentesco;
    }    
}

