<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mandato {
    private $dataInicio;
    private $dataFim;

    public function __construct($dataInicio, $dataFim){
        $this->dataInicio = $dataInicio;
        $this->dataFim = $dataFim;
    }

    public function getDataInicio(){
        return $this->dataInicio;
    }
    
    public function getDataFim(){
        return $this->dataFim;
    }

    public function setDataInicio($dataInicio){
        $this->dataInicio = $dataInicio;
    }    

    public function setDataFim($dataFim){
        $this->dataFim = $dataFim;
    }
    
}