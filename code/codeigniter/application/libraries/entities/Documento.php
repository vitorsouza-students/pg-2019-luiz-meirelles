<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documento {
    private $tituloDocumento;
    private $nomeArquivo;
    private $dataCadastro;

    public function __construct($tituloDocumento, $nomeArquivo, $dataCadastro){
        $this->tituloDocumento = $tituloDocumento;
        $this->nomeArquivo = $nomeArquivo;
        $this->dataCadastro = $dataCadastro;
    }

    public function getTituloDocumento(){
        return $this->tituloDocumento;
    }

    public function getNomeArquivo(){
        return $this->nomeArquivo;
    }

    public function getDataCadastro(){
        return $this->dataCadastro;
    }


    public function setTituloDocumento($tituloDocumento){
        $this->tituloDocumento = $tituloDocumento;
    }

    public function setNomeArquivo($nomeArquivo){
        $this->nomeArquivo = $nomeArquivo;
    }

    public function setDataCadastro($dataCadastro){
        $this->dataCadastro = $dataCadastro;
    }
}