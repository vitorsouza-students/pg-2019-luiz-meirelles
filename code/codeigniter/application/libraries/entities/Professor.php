<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professor extends Pessoa{
    private $parentes;
    private $mandatos;
    private $afastamentosSolicitados;
    private $afastamentosRelator;

    public function __construct($nome, $sobrenome, $email, $telefone, $parentes, $mandatos, $afastamentosSolicitados, $afastamentosRelator) {
        parent::__construct($nome, $sobrenome, $email, $telefone);
        $this->$parentes = $parentes;
        $this->$mandatos = $mandatos;
        $this->$afastamentosSolicitados = $afastamentosSolicitados;
        $this->$afastamentosRelator = $afastamentosRelator;
    }

    public function getParentes(){
        return $this->parentes;
    }
    
    public function getMandato(){
        return $this->mandato;
    }

    public function getAfastamentosSolicitados(){
        return $this->afastamentosSolicitados;
    }

    public function getAfastamentosRelator(){
        return $this->afastamentosRelator;
    }

    public function setParentes($parentes){
        $this->parentes = $parentes;
    }

    public function setMandato($mandatos){
        $this->mandatos = $mandatos;
    }

    public function setAfastamentosSolicitados($afastamentosSolicitados){
        $this->afastamentosSolicitados = $afastamentosSolicitados;
    }

    public function setAfastamentosRelator($afastamentosRelator){
        $this->afastamentosRelator = $afastamentosRelator;
    }
}