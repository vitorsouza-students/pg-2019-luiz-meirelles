<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoa {
    private $nome;
    private $email;
    private $telefone;
    private $sobrenome;
    
    public function __construct($nome, $sobrenome, $email, $telefone){
        $this->nome = $nome;
        $this->email = $email;
        $this->telefone = $telefone;
        $this->sobrenome = $sobrenome;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getTelefone(){
        return $this->telefone;
    }

    public function getSobrenome(){
        return $this->sobrenome;
    }

    public function setNome($nome){
        $this->nome = $nome;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }

    public function setSobrenome($sobrenome){
        $this->sobrenome = $sobrenome;
    }
}