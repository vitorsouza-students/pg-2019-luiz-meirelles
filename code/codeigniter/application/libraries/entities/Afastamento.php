<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afastamento {
    private $dataSolicitacao;
    private $dataInicio;
    private $dataFim;
    private $situacaoSolicitacao;
    private $tipoAfastamento;
    private $motivoAfastamento;
    private $onus;
    private $dataInicioEvento;
    private $dataFimEvento;
    private $nomeEvento;

    public function __construct($dataSolicitacao, $dataInicio, $dataFim, $situacaoSolicitacao, $tipoAfastamento, $motivoAfastamento, $onus, $dataInicioEvento, $dataFimEvento, $nomeEvento){
        $this->dataSolicitacao = $dataSolicitacao;
        $this->dataInicio = $dataInicio;
        $this->dataFim = $dataFim;
        $this->situacaoSolicitacao = $situacaoSolicitacao;
        $this->tipoAfastamento = $tipoAfastamento;
        $this->motivoAfastamento = $motivoAfastamento;
        $this->onus = $onus;
        $this->dataInicioEvento = $dataInicioEvento;
        $this->dataFimEvento = $dataFimEvento;
        $this->nomeEvento = $nomeEvento;
    }

    public function getDataSolicitacao(){
        return $this->dataSolicitacao;
    }

    public function getDataInicio(){
        return $this->dataInicio;
    }
    
    public function getDataFim(){
        return $this->dataFim;
    }

    public function getSituacaoSolicitacao(){
        return $this->situacaoSolicitacao;
    }

    public function getTipoAfastamento(){
        return $this->tipoAfastamento;
    }

    public function getMotivoAfastamento(){
        return $this->motivoAfastamento;
    }

    public function getOnus(){
        return $this->onus;
    }

    public function getDataInicioEvento(){
        return $this->dataInicioEvento;
    }

    public function getDataFimEvento(){
        return $this->dataFimEvento;
    }

    public function getNomeEvento(){
        return $this->nomeEvento;
    }


    public function setDataSolicitacao($dataSolicitacao){
        $this->dataSolicitacao = $dataSolicitacao;
    }

    public function setDataInicio($dataInicio){
        $this->dataInicio = $dataInicio;
    }
    
    public function setDataFim($dataFim){
        $this->dataFim = $dataFim;
    }

    public function setSituacaoSolicitacao($situacaoSolicitacao){
        $this->situacaoSolicitacao = $situacaoSolicitacao;
    }

    public function setTipoAfastamento($tipoAfastamento){
        $this->tipoAfastamento = $tipoAfastamento;
    }

    public function setMotivoAfastamento($motivoAfastamento){
        $this->motivoAfastamento = $motivoAfastamento;
    }

    public function setOnus($onus){
        $this->onus = $onus;
    }

    public function setDataInicioEvento($dataInicioEvento){
        $this->dataInicioEvento = $dataInicioEvento;
    }

    public function setDataFimEvento($dataFimEvento){
        $this->dataFimEvento = $dataFimEvento;
    }

    public function setNomeEvento($nomeEvento){
        $this->nomeEvento = $nomeEvento;
    }
}