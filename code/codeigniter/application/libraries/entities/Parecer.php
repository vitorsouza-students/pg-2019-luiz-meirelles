<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parecer {
    private $dataParecer;
    private $julgamento;
    private $motivo;

    public function __construct($dataParecer, $julgamento, $motivo){
        $this->dataParecer = $dataParecer;
        $this->julgamento = $julgamento;
        $this->motivo = $motivo;
    }
    
    public function getDataParecer(){
        return $this->dataParecer;
    }

    public function getJulgamento(){
        return $this->julgamento;
    }

    public function getMotivo(){
        return $this->motivo;
    }


    public function setDataParecer($dataParecer){
        $this->dataParecer = $dataParecer;
    }

    public function setJulgamento($julgamento){
        $this->julgamento = $julgamento;
    }

    public function setMotivo($motivo){
        $this->motivo = $motivo;
    }
}