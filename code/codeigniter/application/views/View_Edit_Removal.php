<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </head>

    <body>
        <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/Controller_Removal/updateRemoval'?>/<?php echo $row->id; ?>">
            <fieldset>
                <!-- Form Name -->
                <legend>Editar Cadastro de Afastamento</legend>
                
                <!-- Date input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Data Solicitação</label>  
                    <div class="col-md-4">
                        <input id="data_solicitacao" name="data_solicitacao" value="<?php echo $row->data_solicitacao; ?>" type="date" placeholder="Data Solicitação" class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Date input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Data Início Afastamento</label>  
                    <div class="col-md-4">
                        <input id="data_inicio_afastamento" name="data_inicio_afastamento" value="<?php echo $row->data_inicio_afastamento; ?>" type="date" placeholder="Data Início Afastamento" class="form-control input-md" required="">
                    </div>
                </div>

                 <!-- Date input-->
                 <div class="form-group">
                    <label class="col-md-4 control-label" for="">Data Fim Afastamento</label>  
                    <div class="col-md-4">
                        <input id="data_fim_afastamento" name="data_fim_afastamento" value="<?php echo $row->data_fim_afastamento; ?>" type="date" placeholder="Data Fim Afastamento" class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Select input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Tipo Afastamento</label>  
                    <div class="col-md-4">
                       <select name="tipo_afastamento">
                            <option><?php echo $row->tipo_afastamento; ?></option>
                            <option>internacional</option>
                            <option>nacional</option>
                        </select>
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="sobrenome">Motivo Afastamento</label>  
                    <div class="col-md-4">
                        <input id="motivo" name="motivo" value="<?php echo $row->motivo_afastamento; ?>" type="text" placeholder="Motivo" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Select input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Ônus</label>  
                    <div class="col-md-4">
                        <select name="onus">
                            <option><?php echo $row->onus; ?></option>
                            <option>total</option>
                            <option>parcial</option>
                            <option>inexistente</option>
                        </select>
                    </div>
                </div>

                <!-- Date input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Data Início Evento</label>  
                    <div class="col-md-4">
                        <input id="data_inicio_evento" name="data_inicio_evento" value="<?php echo $row->data_inicio_evento; ?>" type="date" placeholder="Data Início Evento" class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Date input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Data Fim Evento</label>  
                    <div class="col-md-4">
                        <input id="data_fim_evento" name="data_fim_evento" value="<?php echo $row->data_fim_evento; ?>" type="date" placeholder="Data Fim Evento" class="form-control input-md" required="">
                    </div>
                </div>
              
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="telefone">Nome Evento</label>  
                    <div class="col-md-4">
                        <input id="nome_evento" name="nome_evento" value="<?php echo $row->nome_evento; ?>" type="text" placeholder="Nome Evento" class="form-control input-md">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
                    </div>
                </div>

            </fieldset>

        </form>
    </body>
</html>