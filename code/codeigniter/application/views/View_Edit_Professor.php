<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </head>

    <body>
        <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/Controller_Professor/updateTeacher'?>/<?php echo $row->id; ?>">
            <fieldset>
                <!-- Form Name -->
                <legend>Editar Cadastro de Professor</legend>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for=""></label>  
                    <div class="col-md-4">
                        <input id="nome" name="nome" value="<?php echo $row->nome; ?>" type="text" placeholder="Nome" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="sobrenome"></label>  
                    <div class="col-md-4">
                        <input id="sobrenome" name="sobrenome" value="<?php echo $row->sobrenome; ?>" type="text" placeholder="Sobrenome" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email"></label>  
                    <div class="col-md-4">
                        <input id="email" name="email" value="<?php echo $row->email; ?>" type="text" placeholder="E-mail" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="telefone"></label>  
                    <div class="col-md-4">
                        <input id="telefone" name="telefone" value="<?php echo $row->telefone; ?>" type="text" placeholder="Telefone" class="form-control input-md">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
                    </div>
                </div>

            </fieldset>

        </form>
    </body>
</html>