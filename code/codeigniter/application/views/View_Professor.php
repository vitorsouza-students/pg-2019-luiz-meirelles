<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title><?php echo $title; ?></title>
    </head>

    <body>
        <?php echo base_url() ?>
        <h3>Cadastro de Professor</h3>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telefone</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($allTeachers as $row) {?>
                    <tr>
                        <th scope="row"><?php echo $row->id; ?></th>
                        <td><?php echo $row->nome; ?></td>
                        <td><?php echo $row->sobrenome; ?></td>
                        <td><?php echo $row->email; ?></td>
                        <td><?php echo $row->telefone; ?></td>
                        <td><a href="<?php echo base_url().'index.php/Controller_Professor/editTeacher';?>/<?php echo $row->id; ?>"> Editar </a> |
                            <a href="<?php echo base_url().'index.php/Controller_Professor/deleteTeacher';?>/<?php echo $row->id; ?>"> Apagar </a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php 
            if($formError){
                echo '<p>'.$formError.'</p>';
            }

            echo form_open('Controller_Professor/insertTeacher');
            echo form_input('nome', set_value('nome'), 'placeholder = "Nome"');
            echo form_input('sobrenome', set_value('sobrenome'), 'placeholder = "Sobrenome"');
            echo form_input('email', set_value('email'), 'placeholder = "Email"');
            //echo form_input('telefone', set_value('telefone'), 'class = "form-control input-md"', 'placeholder = "Telefone"');
            echo form_input(['name' => 'telefone', 'id' => 'telefone', 'value' => set_value('telefone'), 'placeholder' => 'Telefone']);
            echo form_submit('enviar', 'Cadastrar', 'class="btn btn-primary"');
            echo form_close();
        ?>
        
        <h3>Parentesco</h3>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Tipo Parentesco</th>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($allRelatives as $row) {?>
                    <tr>
                        <th><?php echo $row->id_professor1;?></th>
                        <td><?php echo $row->nome_professor1;?></td>
                        <td><?php echo $row->sobrenome_professor1;?></td>
                        <td><?php echo $row->tipo_parentesco;?></td>
                        <th><?php echo $row->id_professor2;?></th>
                        <td><?php echo $row->nome_professor2;?></td>
                        <td><?php echo $row->sobrenome_professor2;?></td>
                        <!-- $row->id é o id do tipo Parentesco -->
                        <td><a href="<?php echo base_url().'index.php/Controller_Professor/deleteRelative';?>/<?php echo $row->id_professor1."-".$row->id."-".$row->id_professor2;?>"> Apagar </a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php
            if($formRelativeError){
                    echo '<p>'.$formRelativeError.'</p>';
            }
        ?>
        <form action="<?php echo base_url().'index.php/Controller_Professor/insertRelative'?>" method="post">
            <select name="id_professor1">
                <?php foreach($allTeachers as $row) {?>
                    <option><?php echo $row->id; ?></option>
                <?php } ?>
            </select>

            <select name="tipo_parentesco">
                <?php foreach($allTypeRelationship as $row) {?>
                    <option><?php echo $row->tipo_parentesco; ?></option>
                <?php } ?>
            </select>
            <select name="id_professor2">
                <?php foreach($allTeachers as $row) {?>
                    <option><?php echo $row->id; ?></option>
                <?php } ?>
            </select>

            <input type="submit" name = "Cadastrar" value="Cadastrar" class="btn btn-primary">
        </form>

        </body>
    </html>