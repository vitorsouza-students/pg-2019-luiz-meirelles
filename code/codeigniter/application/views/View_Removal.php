<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title><?php echo $title; ?></title>
    </head>

    <body>
        <?php echo base_url() ?>
        <h3>Afastamentos</h3>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Data Solicitação</th>
                    <th scope="col">Data Início Afastamento</th>
                    <th scope="col">Data Fim Afastamento</th>
                    <th scope="col">Tipo Afastamento</th>
                    <th scope="col">Motivo</th>
                    <th scope="col">Ônus</th>
                    <th scope="col">Data Início Evento</th>
                    <th scope="col">Data Fim Evento</th>
                    <th scope="col">Nome do Evento</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($allRemovals as $row) {?>
                    <tr>
                        <th scope="row"><?php echo $row->id; ?></th>
                        <td><?php echo $row->data_solicitacao; ?></td>
                        <td><?php echo $row->data_inicio_afastamento; ?></td>
                        <td><?php echo $row->data_fim_afastamento; ?></td>
                        <td><?php echo $row->tipo_afastamento; ?></td>
                        <td><?php echo $row->motivo_afastamento; ?></td>
                        <td><?php echo $row->onus; ?></td>
                        <td><?php echo $row->data_inicio_evento; ?></td>
                        <td><?php echo $row->data_fim_evento; ?></td>
                        <td><?php echo $row->nome_evento; ?></td>
                        <td><a href="<?php echo base_url().'index.php/Controller_Removal/editRemoval';?>/<?php echo $row->id; ?>"> Editar </a> |
                            <a href="<?php echo base_url().'index.php/Controller_Removal/deleteRemoval';?>/<?php echo $row->id; ?>"> Apagar </a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php 
            if($formRemovalAlert){
                echo '<p>'.$formRemovalAlert.'</p>';
            }
        ?>
            <form action="<?php echo base_url().'index.php/Controller_Removal/insertRemoval'?>" method="post"><br>
                Data Solicitação: <input type="date" name="data_solicitacao"><br>
                Data Ínicio Afastamento: <input type="date" name="data_inicio_afastamento"><br>
                Data Fim Afastasmento: <input type="date" name="data_fim_afastamento"><br>
                Tipo Afastamento: <select name="tipo_afastamento">
                    <option>internacional</option>
                    <option>nacional</option>
                </select><br>
                Motivo: <input type="text" name="motivo"><br>
                Ônus: <select name="onus">
                    <option>total</option>
                    <option>parcial</option>
                    <option>inexistente</option>
                </select><br>
                Data Ínicio Evento: <input type="date" name="data_inicio_evento"><br>
                Data Fim Evento: <input type="date" name="data_fim_evento"><br>
                Nome do Evento: <input type="text" name="nome_evento"><br>
                <input type="submit" name = "Cadastrar" value="Cadastrar" class="btn btn-primary">
            
            </form>

            <h3>Solicitações</h3>
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Professor ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">Afastamento ID</th>
                    </tr>
                </thead>
            <tbody>
                <?php foreach($allRequests as $row) {?>
                    <tr>
                        <th scope="row"><?php echo $row->requestId; ?></th>
                        <td><?php echo $row->teacherId; ?></td>
                        <td><?php echo $row->nome; ?></td>
                        <td><?php echo $row->sobrenome; ?></td>
                        <td><?php echo $row->removal_id; ?></td>
                        <td><a href="<?php echo base_url().'index.php/Controller_Removal/deleteRequest';?>/<?php echo $row->requestId; ?>"> Apagar </a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php 
            if($formRequestAlert){
                echo '<p>'.$formRequestAlert.'</p>';
            }
        ?>
        <form action="<?php echo base_url().'index.php/Controller_Removal/insertRequest'?>" method="post"><br>
                Professor: <select name="field_id_teacher">
                                <?php foreach($allTeachers as $row) {?>
                                    <option><?php echo $row->id." ".$row->nome." ".$row->sobrenome; ?></option>
                                <?php } ?>
                            </select>
                Afastamento: <select name="field_id_removal">
                                <?php foreach($allRemovals as $row) {?>
                                    <option><?php echo $row->id; ?></option>
                                <?php } ?>
                            </select>
                <input type="submit" name = "Cadastrar" value="Cadastrar" class="btn btn-primary">
            </form>

            <h3>Relatores</h3>
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Professor ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">Afastamento ID</th>
                    </tr>
                </thead>
            <tbody>
                <?php foreach($allRapporteurs as $row) {?>
                    <tr>
                        <th scope="row"><?php echo $row->rapporteursId; ?></th>
                        <td><?php echo $row->teacherId; ?></td>
                        <td><?php echo $row->nome; ?></td>
                        <td><?php echo $row->sobrenome; ?></td>
                        <td><?php echo $row->removal_id; ?></td>
                        <td><a href="<?php echo base_url().'index.php/Controller_Removal/deleteRapporteur';?>/<?php echo $row->rapporteursId; ?>"> Apagar </a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php 
            if($formRapporteursAlert){
                echo '<p>'.$formRapporteursAlert.'</p>';
            }
        ?>
        <form action="<?php echo base_url().'index.php/Controller_Removal/insertRapporteurs'?>" method="post"><br>
                Professor: <select name="field_id_teacher">
                                <?php foreach($allTeachers as $row) {?>
                                    <option><?php echo $row->id." ".$row->nome." ".$row->sobrenome; ?></option>
                                <?php } ?>
                            </select>
                Afastamento: <select name="field_id_removal">
                                <?php foreach($allRemovals as $row) {?>
                                    <option><?php echo $row->id; ?></option>
                                <?php } ?>
                            </select>
                <input type="submit" name = "Cadastrar" value="Cadastrar" class="btn btn-primary">
            </form>

        </body>
    </html>