<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Professor extends CI_Controller {
    public function __construct(){
        parent:: __construct();
        $this->load->model('Model_Professor');
    }
    
    public function index(){
        $data['title'] = 'Cadastro de Professor';
        $data['formError'] = NULL;
        $data['formRelativeError'] = NULL;
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRelatives'] = $this->Model_Professor->getAllRelatives();
        $data['allTypeRelationship'] = $this->Model_Professor->getAllTypeRelationship();

        $this->load->view('View_Professor.php', $data);
    }

    public function insertTeacher(){
        
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');
        
        if($this->form_validation->run() == FALSE){
            $data['formError'] = validation_errors();
        }else{
            $this->Model_Professor->insertTeacher();
            $data['formError'] = 'Dados Validados com Sucesso.';
        }
        
        $data['title'] = 'Cadastro de Professor';
        $data['formRelativeError'] = NULL;
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRelatives'] = $this->Model_Professor->getAllRelatives();
        $data['allTypeRelationship'] = $this->Model_Professor->getAllTypeRelationship();
        $this->load->view('View_Professor.php', $data);
    }

    public function editTeacher($id){
        $data['row'] = $this->Model_Professor->getTeacher($id);
        $this->load->view('View_Edit_Professor.php', $data);
    }

    public function updateTeacher($id){
        $this->Model_Professor->updateTeacher($id);
        redirect("Controller_Professor");
    }

    public function deleteTeacher($id){

        if($this->Model_Professor->deleteTeacher($id) == 1){
            $data['formError'] = 'Professor com Parentesco Cadastrado. Impossível Remover.';
        }else{
            $data['formError'] = 'Professor removido com Sucesso.';
        }

        $data['title'] = 'Cadastro de Professor';
        $data['formRelativeError'] = NULL;
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRelatives'] = $this->Model_Professor->getAllRelatives();
        $data['allTypeRelationship'] = $this->Model_Professor->getAllTypeRelationship();
        $this->load->view('View_Professor.php', $data);
    }

    public function insertRelative(){
        $data['title'] = 'Cadastro de Professor';
        $data['formRelativeError'] = NULL;

        //die( empty($_POST['id_professor1']) );

        if( empty($_POST['id_professor1']) == 1){
            $data['formRelativeError'] = 'Campo Professor 1 está vazio.';
        }else if( empty($_POST['id_professor2']) == 1){
            $data['formRelativeError'] = 'Campo Professor 2 está vazio.';
        }else if($_POST['id_professor1'] == $_POST['id_professor2']){
            $data['formRelativeError'] = 'Um professor não pode ser parente de si mesmo.';
        }

        if($data['formRelativeError'] == NULL){
            $this->Model_Professor->insertRelative();
            $data['formRelativeError'] = 'Parentesco Cadastrado com Sucesso.';
        }

        $data['formError'] = NULL;
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRelatives'] = $this->Model_Professor->getAllRelatives();
        $data['allTypeRelationship'] = $this->Model_Professor->getAllTypeRelationship();
        $this->load->view('View_Professor.php', $data);
    }
   
   public function deleteRelative($relationship){
       $splittedRelationship = explode("-",$relationship);
       $this->Model_Professor->deleteRelative($splittedRelationship[0], $splittedRelationship[1], $splittedRelationship[2]);
       redirect("Controller_Professor");
   }

}