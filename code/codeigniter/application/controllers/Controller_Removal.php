<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Removal extends CI_Controller {
    public function __construct(){
        parent:: __construct();
        $this->load->model('Model_Professor');
        $this->load->model('Model_Removal');
    }
    
    public function index(){
        $data['title'] = 'Cadastro de Afastamentos';
        $data['allRemovals'] = $this->Model_Removal->getAllRemovals();
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRequests'] = $this->Model_Removal->getAllRequests();
        $data['allRapporteurs'] = $this->Model_Removal->getAllRapporteurs();
        $data['formRemovalAlert'] = NULL; 
        $data['formRequestAlert'] = NULL;
        $data['formRapporteursAlert'] = NULL;

        $this->load->view('View_Removal.php',$data);
    }

    public function insertRemoval(){
        
        $data['formRemovalAlert'] = NULL;
        
        if($_POST['data_solicitacao'] == NULL){
            $data['formRemovalAlert'] = "Data Solicitação Requerida."; 
        }
        
        if($_POST['data_inicio_afastamento'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Data Ínicio Afastamento Requerida."; 
        }

        if($_POST['data_fim_afastamento'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Data Fim Afastamento Requerida."; 
        }

        if($_POST['motivo'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Motivo Rquerido."; 
        }

        if($_POST['data_inicio_evento'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Data Ínicio Evento Requerida."; 
        }

        if($_POST['data_fim_evento'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Data Fim Evento Requerida."; 
        }

        if($_POST['nome_evento'] == NULL){
            $data['formRemovalAlert'] = $data['formRemovalAlert']." Nome Evento Requerido."; 
        }

        if($_POST['data_inicio_afastamento'] > $_POST['data_fim_afastamento']){
            $data['formRemovalAlert'] = $data['formRemovalAlert']."Data do Ínicio do Afastamento não pode ser menor que a data Final.";
        }

        if($data['formRemovalAlert'] == NULL){
            $this->Model_Removal->insertRemoval();
            $data['formRemovalAlert'] = "Afastamento Cadastrado com sucesso.";
        }


        $data['title'] = 'Cadastro de Afastamentos';
        $data['allRemovals'] = $this->Model_Removal->getAllRemovals();
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRequests'] = $this->Model_Removal->getAllRequests();
        $data['allRapporteurs'] = $this->Model_Removal->getAllRapporteurs();
        $data['formRequestAlert'] = NULL;
        $data['formRapporteursAlert'] = NULL;

        $this->load->view('View_Removal.php',$data);
    }

    public function editRemoval($id){
        $data['row'] = $this->Model_Removal->getRemoval($id);
        $this->load->view('View_Edit_Removal.php', $data);
    }

    public function updateRemoval($id){
        $this->Model_Removal->updateRemoval($id);
        redirect("Controller_Removal");
    }

    public function deleteRemoval($id){

        if($this->Model_Removal->deleteRemoval($id) == 1){
            $data['formRemovalAlert'] = 'Afastamento solicitado. Impossível Remover.';
        }else if($this->Model_Removal->deleteRemoval($id) == 2){
            $data['formRemovalAlert'] = 'Afastamento com relator. Impossível Remover.';
        }else{
            $data['formRemovalAlert'] = 'Afastamento removido com Sucesso.';
        }

        $data['title'] = 'Cadastro de Afastamentos';
        $data['allRemovals'] = $this->Model_Removal->getAllRemovals();
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRequests'] = $this->Model_Removal->getAllRequests();
        $data['allRapporteurs'] = $this->Model_Removal->getAllRapporteurs();
        $data['formRequestAlert'] = NULL;
        $data['formRapporteursAlert'] = NULL;

        $this->load->view('View_Removal.php',$data);
    }

    public function insertRequest(){
        $data['formRequestAlert'] = NULL;
        $field_id_teacher = explode(" ",$_POST['field_id_teacher']);

        if( empty($_POST['field_id_teacher']) == 1){
            $data['formRequestAlert'] = 'Não há nenhum Professor cadastrado.';
        }else if( empty($_POST['field_id_removal']) == 1){
            $data['formRequestAlert'] = 'Não há nenhum Afastamento cadastrado.';
        } else if($this->Model_Removal->checkRequestExists($field_id_teacher[0], $_POST['field_id_removal'])->requestExists == 1){
            $data['formRequestAlert'] = 'Professor com Afastamento já cadastrado.';
        } else if($this->Model_Removal->checkRemovalRequested($_POST['field_id_removal'])->removalRequested == 1){
            $data['formRequestAlert'] = 'Afastamento já solicitado.';
        }else{
            $this->Model_Removal->insertRequest();
            $data['formRequestAlert'] = 'Inserido com Sucesso';
        }

        $data['formRemovalAlert'] = NULL;
        $data['title'] = 'Cadastro de Afastamentos';
        $data['allRemovals'] = $this->Model_Removal->getAllRemovals();
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRequests'] = $this->Model_Removal->getAllRequests();
        $data['allRapporteurs'] = $this->Model_Removal->getAllRapporteurs();
        $data['formRapporteursAlert'] = NULL;

        $this->load->view('View_Removal.php',$data);
    }

    public function deleteRequest($id){
        $this->Model_Removal->deleteRequest($id);
        redirect("Controller_Removal");
    }

    public function deleteRapporteur($id){
        $this->Model_Removal->deleteRapporteur($id);
        redirect("Controller_Removal");
    }

    public function insertRapporteurs(){
        $data['formRapporteursAlert'] = NULL;
        $field_id_teacher = explode(" ",$_POST['field_id_teacher']);

        if( empty($_POST['field_id_teacher']) == 1){
            $data['formRapporteursAlert'] = 'Não há nenhum Professor cadastrado.';
        }else if( empty($_POST['field_id_removal']) == 1){
            $data['formRapporteursAlert'] = 'Não há nenhum Afastamento cadastrado.';
        } else if($this->Model_Removal->checkRelative($field_id_teacher[0], $_POST['field_id_removal'])->relativeExists == 1){
            $data['formRapporteursAlert'] = 'Professor com Parentesco ao Solicitante. Não pode ser relator.';
        }else if ($this->Model_Removal->checkRequestOwner($field_id_teacher[0], $_POST['field_id_removal'])->rapporteursOwner == 1){
            $data['formRapporteursAlert'] = 'Um professor não pode Relator do seu próprio Afastamento.';
        }else if ( sizeof($this->Model_Removal->checkRapporteursExists($_POST['field_id_removal'])) != 0){
            $data['formRapporteursAlert'] = 'Já há relator para esse afastamento.';    
        }else{    
            $this->Model_Removal->insertRapporteurs($field_id_teacher[0], $_POST['field_id_removal']);
            $data['formRapporteursAlert'] = 'Inserido com Sucesso';
        }

        $data['formRemovalAlert'] = NULL;
        $data['title'] = 'Cadastro de Afastamentos';
        $data['allRemovals'] = $this->Model_Removal->getAllRemovals();
        $data['allTeachers'] = $this->Model_Professor->getAllTeachers();
        $data['allRequests'] = $this->Model_Removal->getAllRequests();
        $data['allRapporteurs'] = $this->Model_Removal->getAllRapporteurs();
        $data['formRequestAlert'] = NULL;

        $this->load->view('View_Removal.php',$data);
    }
}